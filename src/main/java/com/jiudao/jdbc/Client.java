package com.jiudao.jdbc;

import java.sql.Connection;
import java.sql.Driver;
import java.util.Properties;

import com.jiudao.util.PropertyHelper;

public class Client {
	static PropertyHelper ph=new PropertyHelper("/config.properties");
	static String driverClassName = ph.getProperties("jdbc.driverClassName");
	static String user = ph.getProperties("jdbc.username");
	static String password = ph.getProperties("jdbc.password");
	static String url = ph.getProperties("jdbc.url");
	private Connection conn;

	public Connection getConnection() {
		if(conn != null)return conn;
		try {
			// 加载JDBC驱动程序
			Driver driver = (Driver) Class.forName(driverClassName).newInstance();
			// 配置登录Kylin的用户名和密码
			Properties info = new Properties();
			info.put("user", user);
			info.put("password", password);
			// 连接Kylin服务
			conn = driver.connect(url, info);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
}
