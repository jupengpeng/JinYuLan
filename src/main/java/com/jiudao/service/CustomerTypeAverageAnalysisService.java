package com.jiudao.service;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jiudao.dao.CustomerTypeAverageAnalysisDao;

/**
 * 客群件均分析
 * 
 * @author gyj
 *
 */
@Service
public class CustomerTypeAverageAnalysisService {

	private static final String SUNSHINE_YOUTH = "阳光青年";
	private static final String YOUNG_PEOPLE = "精英才俊";
	private static final String GREAT_FAMILY = "家大业大";
	private static final String WELL_FAMILY = "小康之家";
	private static final String CLASSTYPE_A = "意外";
	private static final String CLASSTYPE_B = "重疾健康";
	private static final String CLASSTYPE_C = "分红";
	private static final String CLASSTYPE_D = "寿险";

	@Autowired
	private CustomerTypeAverageAnalysisDao customerTypeAverageAnalysisDao;

	private final Gson gson = new Gson();
	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 保单总数，投保人总数，总保费
	 * 
	 * @param param
	 * @return
	 */
	public JsonObject getAverage(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		return customerTypeAverageAnalysisDao.average(branch, beginDate, endDate);
	}

	/**
	 * 四大客群客均件数
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFourTypeAveragePolicyno(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		JsonArray jsonArray = customerTypeAverageAnalysisDao.fourTypeAveragePolicyno(branch, beginDate, endDate);
		JsonArray array = new JsonArray();
		JsonObject object1 = new JsonObject();
		JsonObject object2 = new JsonObject();
		JsonObject object3 = new JsonObject();
		JsonObject object4 = new JsonObject();
		object1.addProperty("name", SUNSHINE_YOUTH);
		object1.addProperty("value", 0.0);
		object2.addProperty("name", YOUNG_PEOPLE);
		object2.addProperty("value", 0.0);
		object3.addProperty("name", GREAT_FAMILY);
		object3.addProperty("value", 0.0);
		object4.addProperty("name", WELL_FAMILY);
		object4.addProperty("value", 0.0);
		for (JsonElement jsonElement : jsonArray) {
			if (!(jsonElement.getAsJsonObject().get("PERSON_MARK").isJsonNull())) {
				String string = jsonElement.getAsJsonObject().get("PERSON_MARK").getAsString();
				if (string.equals(SUNSHINE_YOUTH)) {
					if (!(jsonElement.getAsJsonObject().get("COUNT_POLICYNO").isJsonNull())) {
						object1.addProperty("value", jsonElement.getAsJsonObject().get("COUNT_POLICYNO").getAsDouble());
					}
				}
				if (string.equals(YOUNG_PEOPLE)) {
					if (!(jsonElement.getAsJsonObject().get("COUNT_POLICYNO").isJsonNull())) {
						object2.addProperty("value", jsonElement.getAsJsonObject().get("COUNT_POLICYNO").getAsDouble());
					}
				}
				if (string.equals(GREAT_FAMILY)) {
					if (!(jsonElement.getAsJsonObject().get("COUNT_POLICYNO").isJsonNull())) {
						object3.addProperty("value", jsonElement.getAsJsonObject().get("COUNT_POLICYNO").getAsDouble());
					}
				}
				if (string.equals(WELL_FAMILY)) {
					if (!(jsonElement.getAsJsonObject().get("COUNT_POLICYNO").isJsonNull())) {
						object4.addProperty("value", jsonElement.getAsJsonObject().get("COUNT_POLICYNO").getAsDouble());
					}
				}
			}

		}
		array.add(object4);
		array.add(object3);
		array.add(object2);
		array.add(object1);
		return array;
	}

	/**
	 * 四大客群客均保费
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFourTypeAverageMoney(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		JsonArray jsonArray = customerTypeAverageAnalysisDao.fourTypeAverageMoney(branch, beginDate, endDate);
		JsonArray array = new JsonArray();
		JsonObject object1 = new JsonObject();
		JsonObject object2 = new JsonObject();
		JsonObject object3 = new JsonObject();
		JsonObject object4 = new JsonObject();
		object1.addProperty("name", SUNSHINE_YOUTH);
		object1.addProperty("value", 0.0);
		object2.addProperty("name", YOUNG_PEOPLE);
		object2.addProperty("value", 0.0);
		object3.addProperty("name", GREAT_FAMILY);
		object3.addProperty("value", 0.0);
		object4.addProperty("name", WELL_FAMILY);
		object4.addProperty("value", 0.0);
		for (JsonElement jsonElement : jsonArray) {
			if (!(jsonElement.getAsJsonObject().get("PERSON_MARK").isJsonNull())) {
				String string = jsonElement.getAsJsonObject().get("PERSON_MARK").getAsString();
				if (string.equals(SUNSHINE_YOUTH)) {
					if (!(jsonElement.getAsJsonObject().get("SUM_MONEY").isJsonNull())) {
						object1.addProperty("value",
								new BigDecimal(jsonElement.getAsJsonObject().get("SUM_MONEY").getAsDouble() / 1000)
										.setScale(0, BigDecimal.ROUND_HALF_UP));
					}
				}
				if (string.equals(YOUNG_PEOPLE)) {
					if (!(jsonElement.getAsJsonObject().get("SUM_MONEY").isJsonNull())) {
						object2.addProperty("value",
								new BigDecimal(jsonElement.getAsJsonObject().get("SUM_MONEY").getAsDouble() / 1000)
										.setScale(0, BigDecimal.ROUND_HALF_UP));
					}
				}
				if (string.equals(GREAT_FAMILY)) {
					if (!(jsonElement.getAsJsonObject().get("SUM_MONEY").isJsonNull())) {
						object3.addProperty("value",
								new BigDecimal(jsonElement.getAsJsonObject().get("SUM_MONEY").getAsDouble() / 1000)
										.setScale(0, BigDecimal.ROUND_HALF_UP));
					}
				}
				if (string.equals(WELL_FAMILY)) {
					if (!(jsonElement.getAsJsonObject().get("SUM_MONEY").isJsonNull())) {
						object4.addProperty("value",
								new BigDecimal(jsonElement.getAsJsonObject().get("SUM_MONEY").getAsDouble() / 1000)
										.setScale(0, BigDecimal.ROUND_HALF_UP));
					}
				}
			}

		}
		array.add(object4);
		array.add(object3);
		array.add(object2);
		array.add(object1);
		return array;
	}

	/**
	 * 四大客群件均保额
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFourTypeAverageMoneyForClassType(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String personMark = jsonObject.get("personMark").getAsString();
		JsonArray jsonArray = customerTypeAverageAnalysisDao.fourTypeAverageMoneyForClassType(branch, beginDate,
				endDate, personMark);
		JsonArray array = new JsonArray();
		if (jsonArray.size() != 0) {
			JsonObject objectA = new JsonObject();
			objectA.addProperty("name", CLASSTYPE_A);
			double moneyA = 0;
			double countA = 0;
			JsonObject objectB = new JsonObject();
			objectB.addProperty("name", CLASSTYPE_B);
			double moneyB = 0;
			double countB = 0;
			JsonObject objectC = new JsonObject();
			objectC.addProperty("name", CLASSTYPE_C);
			double moneyC = 0;
			double countC = 0;
			JsonObject objectD = new JsonObject();
			objectD.addProperty("name", CLASSTYPE_D);
			double moneyD = 0;
			double countD = 0;
			for (JsonElement jsonElement : jsonArray) {
				JsonObject object = jsonElement.getAsJsonObject();
				if ((!object.get("CLASSTYPE").isJsonNull())
						&& object.get("CLASSTYPE").toString().indexOf("a") != -1) {
					if (!object.get("SUM_AMOUNT").isJsonNull()) {
						moneyA += object.get("SUM_AMOUNT").getAsDouble();
						countA += object.get("COUNT_POLICYNO").getAsDouble();
					}
				}
				if ((!object.get("CLASSTYPE").isJsonNull())
						&& object.get("CLASSTYPE").toString().indexOf("b") != -1) {
					if (!object.get("SUM_AMOUNT").isJsonNull()) {
						moneyB += object.get("SUM_AMOUNT").getAsDouble();
						countB += object.get("COUNT_POLICYNO").getAsDouble();
					}
				}
				if ((!object.get("CLASSTYPE").isJsonNull())
						&& object.get("CLASSTYPE").toString().indexOf("c") != -1) {
					if (!object.get("SUM_AMOUNT").toString().equals("null")) {
						moneyC += object.get("SUM_AMOUNT").getAsDouble();
						countC += object.get("COUNT_POLICYNO").getAsDouble();
					}
				}
				if ((!object.get("CLASSTYPE").isJsonNull())
						&& object.get("CLASSTYPE").toString().indexOf("d") != -1) {
					if (!object.get("SUM_AMOUNT").toString().equals("null")) {
						moneyD += object.get("SUM_AMOUNT").getAsDouble();
						countD += object.get("COUNT_POLICYNO").getAsDouble();
					}
				}
			}
			// 四舍五入件均保额
			if (countA != 0.0) {
				objectA.addProperty("value",
						new BigDecimal(moneyA / countA / 1000).setScale(0, BigDecimal.ROUND_HALF_UP));
			} else {
				objectA.addProperty("value", 0.0);
			}
			if (countB != 0.0) {
				objectB.addProperty("value",
						new BigDecimal(moneyB / countB / 1000).setScale(0, BigDecimal.ROUND_HALF_UP));
			} else {
				objectB.addProperty("value", 0.0);
			}
			if (countC != 0.0) {
				objectC.addProperty("value",
						new BigDecimal(moneyC / countC / 1000).setScale(0, BigDecimal.ROUND_HALF_UP));
			} else {
				objectC.addProperty("value", 0.0);
			}
			if (countD != 0.0) {
				objectD.addProperty("value",
						new BigDecimal(moneyD / countD / 1000).setScale(0, BigDecimal.ROUND_HALF_UP));
			} else {
				objectD.addProperty("value", 0.0);
			}

			array.add(objectA);
			array.add(objectB);
			array.add(objectC);
			array.add(objectD);
		}
		return array;
	}

}
