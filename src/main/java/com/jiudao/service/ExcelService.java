package com.jiudao.service;



import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.jiudao.util.ExportExcelUtil;

/**
 * 导出excel
 * @author gyj
 *
 */
@Service
public class ExcelService {

	private final Gson gson =new Gson();

	public String out(final String param){
		final JsonObject jsonObject=gson.fromJson(param, JsonObject.class);
		return ExportExcelUtil.exportExcel(jsonObject.getAsJsonObject());
		
	}
	


}
