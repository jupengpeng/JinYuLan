package com.jiudao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.jiudao.dao.MainRiskAnalyseDao;

/**
 * 险种分析 service
 * @author qiong_c
 *
 */
@Service
public class MainRiskAnalyseService {
	@Autowired
	MainRiskAnalyseDao dao;

	/**
	 * 获取前10名险种保单件数
	 * @param num 组合险种件数
	 * @return
	 */
	public JsonArray getTop10Data(String num,String startTime,String endTime,String branchCode){
		return dao.getTop10Data(num,startTime,endTime,branchCode);
	}

	public JsonArray getMainRiskData(String startTime,String endTime,String branchCode){
		return dao.getMainRiskData(startTime,endTime,branchCode);
	}
}
