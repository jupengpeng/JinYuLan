package com.jiudao.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.dao.EmpnoAnalysisDao;
import com.jiudao.dao.PaymentYearsAnalysisDao;

/**
 * 营销员分析
 * 
 * @author gyj
 *
 */
@Service
public class EmpnoAnalysisService {

	@Autowired
	private EmpnoAnalysisDao empnoAnalysisDao;

	private final Gson gson = new Gson();
	private final Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 入职后第一次举绩
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFirstDaysAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		return empnoAnalysisDao.firstDays(branch, beginDate, endDate);
	}

	/**
	 * 入职后转正所需时间
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getRegularDaysAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		return empnoAnalysisDao.regularDays(branch, beginDate, endDate);
	}

	/**
	 * 转正后第一次晋升所需时间
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getPromotionDaysAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		return empnoAnalysisDao.promotionDays(branch, beginDate, endDate);
	}

	/**
	 * 离职人员留存时间
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getStayDaysAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		return empnoAnalysisDao.stayDays(branch, beginDate, endDate);
	}

	/**
	 * 入职后三月平均FYC
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getThreeMonthFycAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String service = jsonObject.get("service").getAsString();
		return empnoAnalysisDao.threeMonthFyc(branch, beginDate, endDate, service);
	}

	/**
	 * 入职后六月平均FYC
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getSixMonthFycAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String service = jsonObject.get("service").getAsString();
		return empnoAnalysisDao.sixMonthFyc(branch, beginDate, endDate, service);
	}

	/**
	 * 平均FYC
	 * 
	 * @param param
	 * @return
	 */
	public String getFycAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String month = jsonObject.get("month").getAsString();//3月或6月
		String service = jsonObject.get("service").getAsString();//是否离职
		if (month.equals("3")) {
			return empnoAnalysisDao.threeMonthFyc(branch, beginDate, endDate, service).toString()
					.replace("THREE_MONTHS_FYC", "name").replace("COUNT_EMPNO", "value");
		}
		return empnoAnalysisDao.sixMonthFyc(branch, beginDate, endDate, service).toString()
				.replace("SIX_MONTHS_FYC", "name").replace("COUNT_EMPNO", "value");
	}

}
