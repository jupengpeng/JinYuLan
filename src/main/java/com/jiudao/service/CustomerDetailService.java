package com.jiudao.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.dao.CustomerDetailDao;

/**
 * 
 * @author jpp
 * 客群详细分析 service
 */
@Service
public class CustomerDetailService {
	
	@Autowired
	private CustomerDetailDao customerDetailDao;
	
	private final Gson gson =new Gson();
	private final Logger logger=Logger.getLogger(this.getClass());
	
	
	/**
	 * @param param
	 * @return String
	 * 客群详细分析（总）
	 */
	public String getallData(final String param){
		
		try {
			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
			return customerDetailDao.getall(jsonParam).toString();
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}
//	/**
//	 * @param param
//	 * @return String
//	 * 四大客群分布查询
//	 */
//	public JsonArray getData(final String param){
//		
//		try {
//			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
//			return customerDetailDao.getFourCustomerAnalysis();
//		} catch (Exception e) {
//			logger.error(e);
//		}
//		return null;
//	}
//	
//	/**
//	 * @param param
//	 * @return String
//	 * 长短险分布
//	 */
//	public JsonArray getCountRiskMark(final String param){
//		
//		try {
//			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
//			return customerDetailDao.getCountRiskMark();
//		} catch (Exception e) {
//			logger.error(e);
//		}
//		return null;
//	}
//	/**
//	 * @param param
//	 * @return String
//	 * 被投保人数
//	 */
//	public JsonArray getPidNum(final String param){
//		
//		try {
//			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
//			return customerDetailDao.getPidNum();
//		} catch (Exception e) {
//			logger.error(e);
//		}
//		return null;
//	}
//	/**
//	 * @param param
//	 * @return String
//	 * 投保人数
//	 */
//	public JsonArray getApidNum(final String param){
//		
//		try {
//			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
//			return customerDetailDao.getApidNum();
//		} catch (Exception e) {
//			logger.error(e);
//		}
//		return null;
//	}
//	/**
//	 * @param param
//	 * @return String
//	 * 老客户数
//	 */
//	public JsonArray getOldNum(final String param){
//		
//		try {
//			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
//			return customerDetailDao.getOldNum();
//		} catch (Exception e) {
//			logger.error(e);
//		}
//		return null;
//	}
//	/**
//	 * @param param
//	 * @return String
//	 * 性别分布
//	 */
//	public JsonArray getSexNum(final String param){
//		
//		try {
//			final JsonObject jsonParam=gson.fromJson(param, JsonObject.class);
//			return customerDetailDao.getSexNum();
//		} catch (Exception e) {
//			logger.error(e);
//		}
//		return null;
//	}
	
	/**
	 * @param param
	 * @return String
	 * 全部险种列举
	 */
	public JsonArray getRiskList(){
		
		try {
			return customerDetailDao.getRiskList();
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}
	
	/**
	 * @param param
	 * @return String
	 * 全部分公司列举
	 */
	public JsonArray getIntorgInfo(){
		
		try {
			return customerDetailDao.getIntorgInfo();
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}
}
