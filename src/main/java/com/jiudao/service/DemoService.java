package com.jiudao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.jiudao.dao.DemoDao;

@Service
public class DemoService {
	@Autowired
	DemoDao demoDao;

	public JsonArray getUser(String id){
		return demoDao.getUser(id);
	}
	public JsonArray getAgg(){
		return demoDao.getMeasureGroupByGender();
	}

}
