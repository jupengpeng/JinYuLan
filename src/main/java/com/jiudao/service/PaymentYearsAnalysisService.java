package com.jiudao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.jiudao.dao.PaymentYearsAnalysisDao;

/**
 * 缴费年限分析service
 * 
 * @author gyj
 *
 */
@Service
public class PaymentYearsAnalysisService {

	@Autowired
	private PaymentYearsAnalysisDao paymentYearsAnalysisDao;

	private final Gson gson = new Gson();
	private static final String ALL_YEAR = "趸交";
	private static final String THREE_YEAR = "3年";
	private static final String FIVE_YEAR = "5年";
	private static final String TEN_FIFTEEN_UP_YEAR = "10年15年及以上";
	private static final String OTHER_YEAR = "其他";

	/**
	 * 保单缴费年限分布
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getPaymentYearsAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString(); // 地区
		String beginDate = jsonObject.get("beginDate").getAsString(); // 起始时间
		String endDate = jsonObject.get("endDate").getAsString(); // 结束时间
		JsonArray jsonArray = paymentYearsAnalysisDao.paymentYearAnalysis(branch, beginDate, endDate);
		JsonObject object1 = new JsonObject();
		object1.addProperty("name", ALL_YEAR);
		int value1 = 0;
		JsonObject object2 = new JsonObject();
		object2.addProperty("name", THREE_YEAR);
		int value2 = 0;
		JsonObject object3 = new JsonObject();
		object3.addProperty("name", FIVE_YEAR);
		int value3 = 0;
		JsonObject object4 = new JsonObject();
		object4.addProperty("name", TEN_FIFTEEN_UP_YEAR);
		int value4 = 0;
		JsonObject object5 = new JsonObject();
		object5.addProperty("name", OTHER_YEAR);
		int value5 = 0;
		for (JsonElement jsonElement : jsonArray) {
			if (!(jsonElement.getAsJsonObject().get("PAYCODE").isJsonNull())) {
				if (jsonElement.getAsJsonObject().get("PAYCODE").getAsString().equals("01")) {
					value1 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
				}
			} else {
				if (!(jsonElement.getAsJsonObject().get("YEARNUM").isJsonNull())) {
					if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 3) {
						value2 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 5) {
						value3 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 10
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 15
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() > 15) {
						value4 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else {
						value5 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					}
				} else {
					value5 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
				}
			}
		}
		JsonArray array = new JsonArray();
		if (value1 != 0) {
			object1.addProperty("value", value1);
			array.add(object1);
		}
		if (value2 != 0) {
			object2.addProperty("value", value2);
			array.add(object2);
		}
		if (value3 != 0) {
			object3.addProperty("value", value3);
			array.add(object3);
		}
		if (value4 != 0) {
			object4.addProperty("value", value4);
			array.add(object4);
		}
		if (value5 != 0) {
			object5.addProperty("value", value5);
			array.add(object5);
		}
		return array;
	}

	/**
	 * 前十主力险种缴费年限分析
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getTopTenPaymentYearAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		JsonArray jsonArray = paymentYearsAnalysisDao.topTenPaymentYearAnalysis(branch, beginDate, endDate);
		// {"COUNT_POLICYNO":"17","CLASSCODE":"44154100","CLASSNAME":"金佑人生终身寿险（分红型）A款（2017版）","COUNT_YEAR":"17","YEARNUM":null,"PAYCODE":null}
		for (JsonElement jsonElement : jsonArray) {
			if (!(jsonElement.getAsJsonObject().get("PAYCODE").isJsonNull())
					&& jsonElement.getAsJsonObject().get("PAYCODE").getAsString().equals("01")) {
				jsonElement.getAsJsonObject().addProperty("YEARNUM", ALL_YEAR);
			} else {
				if (!(jsonElement.getAsJsonObject().get("YEARNUM").isJsonNull())) {
					if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 3) {
						jsonElement.getAsJsonObject().addProperty("YEARNUM", THREE_YEAR);
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 5) {
						jsonElement.getAsJsonObject().addProperty("YEARNUM", FIVE_YEAR);
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 10
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 15
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() > 15) {
						jsonElement.getAsJsonObject().addProperty("YEARNUM", TEN_FIFTEEN_UP_YEAR);
					} else {
						jsonElement.getAsJsonObject().addProperty("YEARNUM", OTHER_YEAR);
					}
				} else {
					jsonElement.getAsJsonObject().addProperty("YEARNUM", OTHER_YEAR);
				}
			}
		}
		return jsonArray;
	}

	/**
	 * 四大客群主要缴费年限
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFourTypePaymentYearAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String personMark = jsonObject.get("personMark").getAsString();
		JsonArray jsonArray = paymentYearsAnalysisDao.fourTypePaymentYearAnalysis(branch, beginDate, endDate,
				personMark);
		JsonObject object1 = new JsonObject();
		object1.addProperty("name", ALL_YEAR);
		int value1 = 0;
		JsonObject object2 = new JsonObject();
		object2.addProperty("name", THREE_YEAR);
		int value2 = 0;
		JsonObject object3 = new JsonObject();
		object3.addProperty("name", FIVE_YEAR);
		int value3 = 0;
		JsonObject object4 = new JsonObject();
		object4.addProperty("name", TEN_FIFTEEN_UP_YEAR);
		int value4 = 0;
		JsonObject object5 = new JsonObject();
		object5.addProperty("name", OTHER_YEAR);
		int value5 = 0;
		for (JsonElement jsonElement : jsonArray) {
			if (!(jsonElement.getAsJsonObject().get("PAYCODE").isJsonNull())
					&& jsonElement.getAsJsonObject().get("PAYCODE").getAsString().equals("01")) {
				value1 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
			} else {
				if (!(jsonElement.getAsJsonObject().get("YEARNUM").isJsonNull())) {
					if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 3) {
						value2 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 5) {
						value3 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 10
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 15
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() > 15) {
						value4 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else {
						value5 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					}
				} else {
					value5 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
				}
			}
		}
		JsonArray array = new JsonArray();
		if (value1 != 0) {
			object1.addProperty("value", value1);
			array.add(object1);
		}
		if (value2 != 0) {
			object2.addProperty("value", value2);
			array.add(object2);
		}
		if (value3 != 0) {
			object3.addProperty("value", value3);
			array.add(object3);
		}
		if (value4 != 0) {
			object4.addProperty("value", value4);
			array.add(object4);
		}
		if (value5 != 0) {
			object5.addProperty("value", value5);
			array.add(object5);
		}
		return array;
	}

	/**
	 * 四大客群前五险种缴费分布
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFourTypeTopFiveAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String personMark = jsonObject.get("personMark").getAsString();
		return paymentYearsAnalysisDao.fourTypeTopFiveAnalysis(branch, beginDate, endDate, personMark);
	}

	/**
	 * 四大客群前五险种缴费分布联动饼图（展示缴费年限）
	 * 
	 * @param param
	 * @return
	 */
	public JsonArray getFourTypeTopFivePaymentYearAnalysis(final String param) {
		final JsonObject jsonObject = gson.fromJson(param, JsonObject.class);
		String branch = jsonObject.get("branch").getAsString();
		String beginDate = jsonObject.get("beginDate").getAsString();
		String endDate = jsonObject.get("endDate").getAsString();
		String personMark = jsonObject.get("personMark").getAsString();//四大客群分类
		String classname = jsonObject.get("classname").getAsString();//险种名称
		String classcode = "";
		JsonArray jArray = paymentYearsAnalysisDao.fourTypeTopFiveAnalysis(branch, beginDate, endDate, personMark);
		for (JsonElement jsonElement : jArray) {//获取前五大险种的classcode
			if (classname == null || classname.equals("")) {
				classcode += "'" + jsonElement.getAsJsonObject().get("CLASSCODE").getAsString() + "',";
			} else {
				if (jsonElement.getAsJsonObject().get("CLASSNAME").getAsString().equals(classname)) {
					classcode += "'" + jsonElement.getAsJsonObject().get("CLASSCODE").getAsString() + "',";
					break;
				}
			}
		}
		JsonArray jsonArray = paymentYearsAnalysisDao.fourTypeTopFivePaymentYearAnalysis(branch, beginDate, endDate,
				personMark, classcode);
		JsonObject object1 = new JsonObject();
		object1.addProperty("name", ALL_YEAR);
		int value1 = 0;
		JsonObject object2 = new JsonObject();
		object2.addProperty("name", THREE_YEAR);
		int value2 = 0;
		JsonObject object3 = new JsonObject();
		object3.addProperty("name", FIVE_YEAR);
		int value3 = 0;
		JsonObject object4 = new JsonObject();
		object4.addProperty("name", TEN_FIFTEEN_UP_YEAR);
		int value4 = 0;
		JsonObject object5 = new JsonObject();
		object5.addProperty("name", OTHER_YEAR);
		int value5 = 0;
		for (JsonElement jsonElement : jsonArray) {
			if (!(jsonElement.getAsJsonObject().get("PAYCODE").isJsonNull())
					&& jsonElement.getAsJsonObject().get("PAYCODE").getAsString().equals("01")) {
				value1 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
			} else {
				if (!(jsonElement.getAsJsonObject().get("YEARNUM").isJsonNull())) {
					if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 3) {
						value2 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 5) {
						value3 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else if (jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 10
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() == 15
							|| jsonElement.getAsJsonObject().get("YEARNUM").getAsInt() > 15) {
						value4 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					} else {
						value5 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
					}
				} else {
					value5 += jsonElement.getAsJsonObject().get("POLICYNO_COUNT").getAsInt();
				}
			}
		}
		JsonArray array = new JsonArray();
		if (value1 != 0) {
			object1.addProperty("value", value1);
			array.add(object1);
		}
		if (value2 != 0) {
			object2.addProperty("value", value2);
			array.add(object2);
		}
		if (value3 != 0) {
			object3.addProperty("value", value3);
			array.add(object3);
		}
		if (value4 != 0) {
			object4.addProperty("value", value4);
			array.add(object4);
		}
		if (value5 != 0) {
			object5.addProperty("value", value5);
			array.add(object5);
		}
		return array;
	}

}
