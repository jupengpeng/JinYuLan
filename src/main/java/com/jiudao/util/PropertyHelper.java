package com.jiudao.util;

import java.io.InputStream;
import java.util.Properties;


public class PropertyHelper {
	Properties prop;
	public PropertyHelper(String path){
		if(path==null)path="/config.properties";
		prop =  new  Properties();    
        InputStream in = this.getClass().getResourceAsStream(path); 
        try  {    
            prop.load(in);    
        }catch(Exception e){
        	System.out.println("Load config.properties has an error!");
        }
	}
	public String getProperties(String key){
		String result = prop.getProperty(key).trim(); 
		return result;
	}
}
