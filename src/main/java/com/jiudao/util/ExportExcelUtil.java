package com.jiudao.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * 导出excel工具类
 * 
 * @author gyj
 *
 */
public class ExportExcelUtil {

	static PropertyHelper ph = new PropertyHelper("/config.properties");
	static String excelPath = ph.getProperties("excel.path");
	private static final Logger logger = Logger.getLogger(ExportExcelUtil.class);

	/**
	 * 导出excel
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static String exportExcel(JsonObject jsonObject) {
		// 第一步创建workbook
		HSSFWorkbook wb = new HSSFWorkbook();
		for (Entry<String, JsonElement> e : jsonObject.entrySet()) {// 循环遍历创建多个sheet
			String sheetName = e.getKey();
			// 第二步创建sheet
			HSSFSheet sheet = wb.createSheet(sheetName);
			// 第三步创建行row:添加表头0行
			HSSFRow row = sheet.createRow(0);
			HSSFCellStyle style = wb.createCellStyle();
			JsonArray array = new JsonArray();
			boolean b = true;
			try {
				double d = jsonObject.get(e.getKey()).getAsDouble();
				HSSFCell cell = row.createCell(0);
				cell.setCellValue(d);
				cell.setCellStyle(style);
				b = false;
			} catch (Exception e2) {
				logger.info("不是数字");
			}
			if (b) {//如果不为数字
				try {// 解决excel接口json格式冲突问题
					array = new Gson().fromJson(jsonObject.get(e.getKey()), JsonArray.class);// 取出每一个图表数据的JsonArray
				} catch (Exception exception) {
					array = new Gson().fromJson(jsonObject.get(e.getKey()).getAsString(), JsonArray.class);// 取出每一个图表数据的JsonArray
				}
				// 第四步创建单元格 (循环生成表头)
				List<String> list = new ArrayList<String>();
				for (JsonElement jsonElement : array) {
					int celli = 0;
					JsonObject object = jsonElement.getAsJsonObject();
					for (Entry<String, JsonElement> m : object.entrySet()) {
						list.add(m.getKey());// 记录所有的key值
						HSSFCell cell = row.createCell(celli);
						cell.setCellValue(m.getKey());
						cell.setCellStyle(style);
						celli++;
					}
					break;
				}

				// 第五步插入数据
				int i = 1;
				for (JsonElement jsonElement : array) {
					JsonObject object = jsonElement.getAsJsonObject();
					// 创建行
					row = sheet.createRow(i);
					// 创建单元格并且添加数据
					for (int j = 0; j < list.size(); j++) {// 循环遍历除list中的key
						row.createCell(j).setCellValue(object.get(list.get(j)).toString());
					}
					i++;
				}
			}

		}
		// 第六步将生成excel文件保存到指定路径下
		String uuid = UUID.randomUUID().toString().replaceAll("-", "");// 文件名使用UUID
		File file = new File(excelPath + uuid + ".xls");
		// 如果文件夹不存在则创建
		 if (!file.getParentFile().exists()) {
		       file.getParentFile().mkdirs();
		      }
		try {
			FileOutputStream fout = new FileOutputStream(file);
			wb.write(fout);
			fout.close();
			logger.info("导出EXCEL成功" + file.getAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return (uuid + ".xls");
	}

}
