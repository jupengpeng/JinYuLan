package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class RiskNum extends BaseEntity{
	
	public RiskNum() {
		this.t_schema="JYLFX";
		this.t_name="RISK_NUM";
	}
	public static final String POLICYNO="POLICYNO";
	public static final String RISK_NUM="RISK_NUM";
}
