package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class Branch extends BaseEntity{
	
	public Branch() {
		this.t_schema="JYLFX";
		this.t_name="BRANCH";
	}
	public static final String BRANCH="BRANCH";
	public static final String INTORGNAME="INTORGNAME";
}
