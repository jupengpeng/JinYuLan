package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class JylRiskconPerson extends BaseEntity {

	public JylRiskconPerson() {
		this.t_schema = "JYLFX";
		this.t_name = "JYL_RISKCON_PERSON";
	}

	public static final String POLICYNO = "POLICYNO"; // 保单号
	public static final String LONG_RISK = "LONG_RISK";// 长短险
	public static final String RISK_NUM = "RISK_NUM";// 险种件数
	public static final String PERSON_MARK = "PERSON_MARK";// 四大客群
	public static final String ALL_AMOUNT = "ALL_AMOUNT";// 保额总数
	public static final String PERSON_MARK_AGE = "PERSON_MARK_AGE";// 客群年龄标志
	public static final String PERSON_MARK_TMOUNT = "PERSON_MARK_TMOUNT";// 客群前一年年缴保费
	public static final String LONG_RISK_DAYS = "LONG_RISK_DAYS";// 长短险天数

}
