package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class RiskMark extends BaseEntity{
	
	public RiskMark() {
		this.t_schema="JYLFX";
		this.t_name="RISK_MARK";
	}
	public static final String POLICYNO="POLICYNO";
	public static final String RISK_MASK="RISK_MASK";
}
