package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class PersonNewOld extends BaseEntity {

	public PersonNewOld() {
		this.t_schema = "JYLFX";
		this.t_name = "PERSON_NEW_OLD";
	}

	public static final String ID = "ID"; // 身份证号
	public static final String CUSTMK = "CUSTMK";// 新老客户标志
	public static final String SEX = "SEX";// 性别

}
