package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class Risklist extends BaseEntity{
	
	public Risklist() {
		this.t_schema="JYLFX";
		this.t_name="JYL_RISKLIST";
	}
	public static final String CLASSCODE="CLASSCODE";
	public static final String CLASSNAME="CLASSNAME";
}
