package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class Empno extends BaseEntity{
	
	public Empno() {
		this.t_schema="JYLFX";
		this.t_name="JYL_EMPNO";
	}
	public static final String EMPNO="EMPNO";
	public static final String INDATE="INDATE";
	public static final String FIRSTDAYS="FIRSTDAYS";
	public static final String REGULARDAYS="REGULARDAYS";
	public static final String PROMOTIONDAYS="PROMOTIONDAYS";
	public static final String STAYDAYS="STAYDAYS";
	public static final String THREE_MONTHS_FYC="THREE_MONTHS_FYC";
	public static final String SIX_MONTHS_FYC="SIX_MONTHS_FYC";
	public static final String BRANCH="BRANCH";
}
