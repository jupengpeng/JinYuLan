package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class RiskType extends BaseEntity{
	
	public RiskType() {
		this.t_schema="JYLBASE";
		this.t_name="RISK_TYPE";
	}
	public static final String CLASSNAME="CLASSNAME";
	public static final String CLASSTYPE="CLASSTYPE";
}
