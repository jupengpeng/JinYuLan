package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class Applist extends BaseEntity{
	
	public Applist() {
		this.t_schema="JYLBASE";
		this.t_name="APPLIST";
	}
	public static final String POLICYNO="POLICYNO";
	public static final String CLASSCODE="CLASSCODE";
	public static final String PAYCODE="PAYCODE";
	public static final String YEARNUM="YEARNUM";
}
