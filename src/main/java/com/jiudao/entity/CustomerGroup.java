package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class CustomerGroup extends BaseEntity{
	
	public CustomerGroup() {
		this.t_schema="JYLFX";
		this.t_name="CUSTOMER_GROUP";
	}
	public static final String POLICYNO="POLICYNO";
	public static final String PERSON_MASK="PERSON_MARK";
}
