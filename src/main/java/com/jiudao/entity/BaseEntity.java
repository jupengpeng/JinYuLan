package com.jiudao.entity;

public class BaseEntity {
	//DB中所属的schema
	protected String t_schema;
	//表名称
	protected String t_name;
	

	public String tablename() {
		return t_schema.concat(".").concat(t_name);
	}
	public String column(String column){
		return t_name.concat(".").concat(column);
	}
	public String getTname() {
		return t_name;
	}
}
