package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class AllAmount extends BaseEntity{
	
	public AllAmount() {
		this.t_schema="JYLFX";
		this.t_name="ALL_AMOUNT";
	}
	public static final String POLICYNO="POLICYNO";
	public static final String CLASSCODE="CLASSCODE";
	public static final String ALL_AMOUNT="ALL_AMOUNT";
}
