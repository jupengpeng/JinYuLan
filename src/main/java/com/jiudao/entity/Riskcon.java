package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class Riskcon extends BaseEntity{
	
	public Riskcon() {
		this.t_schema="JYLBASE";
		this.t_name="RISKCON";
	}
	public static final String APPF="APPF";
	public static final String APPDATE="APPDATE";
	public static final String CLASSCODE="CLASSCODE";
	public static final String BRANCH="BRANCH";
	public static final String POLICYNO="POLICYNO";
	public static final String APID="APID";
	public static final String PID="PID";
	public static final String TMOUNT="TMOUNT";
}
