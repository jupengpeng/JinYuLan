package com.jiudao.entity;

import org.springframework.stereotype.Component;

@Component
public class Moneysch extends BaseEntity{
	
	public Moneysch() {
		this.t_schema="JYLBASE";
		this.t_name="JYL_MONEYSCH";
	}
	public static final String POLICYNO="POLICYNO";
	public static final String CLASSCODE="CLASSCODE";
	public static final String YEARNUM="YEARNUM";
}
