package com.jiudao.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;


@Controller
public abstract class AbstractController {
	protected final Logger logger=Logger.getLogger(this.getClass());
}
