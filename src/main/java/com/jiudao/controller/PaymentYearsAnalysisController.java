package com.jiudao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.service.PaymentYearsAnalysisService;

/**
 * 缴费年限分析controller
 * 
 * @author gyj
 *
 */
@Controller
@RequestMapping("/paymentYearsAnalysis")
public class PaymentYearsAnalysisController extends AbstractController {
	@Autowired
	PaymentYearsAnalysisService paymentYearsAnalysisService;

	/**
	 * 保单缴费年限分析
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getPaymentYearsAnalysis")
	@ResponseBody
	public String getPaymentYearsAnalysis(@RequestBody final String param) {
		return paymentYearsAnalysisService.getPaymentYearsAnalysis(param).toString();
	}

	/**
	 * 前十主力险种缴费年限分析
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getTopTenPaymentYearAnalysis")
	@ResponseBody
	public String getTopTenPaymentYearAnalysis(@RequestBody final String param) {
		return paymentYearsAnalysisService.getTopTenPaymentYearAnalysis(param).toString();
	}

	/**
	 * 四大客群主要缴费年限
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getFourTypePaymentYearAnalysis")
	@ResponseBody
	public String getFourTypePaymentYearAnalysis(@RequestBody final String param) {
		return paymentYearsAnalysisService.getFourTypePaymentYearAnalysis(param).toString();
	}

	/**
	 * 四大客群前五险种缴费分布
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getFourTypeTopFiveAnalysis")
	@ResponseBody
	public String getFourTypeTopFiveAnalysis(@RequestBody final String param) {
		return paymentYearsAnalysisService.getFourTypeTopFiveAnalysis(param).toString();
	}

	/**
	 * 四大客群前五险种缴费分布联动饼图（展示缴费年限）
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getFourTypeTopFivePaymentYearAnalysis")
	@ResponseBody
	public String getFourTypeTopFivePaymentYearAnalysis(@RequestBody final String param) {
		return paymentYearsAnalysisService.getFourTypeTopFivePaymentYearAnalysis(param).toString();
	}

	/**
	 * 缴费年限分析页面(所有数据)
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/allPaymentYearAnalysis")
	@ResponseBody
	public String allPaymentYearAnalysis(@RequestBody final String param) {
		JsonObject jsonObject = new JsonObject();
		//保单缴费年限分析
		JsonArray paymentYears = paymentYearsAnalysisService.getPaymentYearsAnalysis(param);
		//前十主力险种缴费年限分析
		JsonArray topTenPaymentYear = paymentYearsAnalysisService.getTopTenPaymentYearAnalysis(param);
		//四大客群主要缴费年限
		JsonArray fourTypePaymentYear = paymentYearsAnalysisService.getFourTypePaymentYearAnalysis(param);
		//四大客群前五险种缴费分布
		JsonArray fourTypeTopFive = paymentYearsAnalysisService.getFourTypeTopFiveAnalysis(param);
		//四大客群前五险种缴费分布联动饼图（展示缴费年限）
		JsonArray fourTypeTopFivePaymentYear = paymentYearsAnalysisService.getFourTypeTopFivePaymentYearAnalysis(param);
		jsonObject.add("paymentYears", paymentYears);
		jsonObject.add("topTenPaymentYear", topTenPaymentYear);
		jsonObject.add("fourTypePaymentYear", fourTypePaymentYear);
		jsonObject.add("fourTypeTopFive", fourTypeTopFive);
		jsonObject.add("fourTypeTopFivePaymentYear", fourTypeTopFivePaymentYear);
		logger.info(jsonObject.toString());
		return jsonObject.toString();
	}

	/**
	 * 联动饼图
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getTopFive")
	@ResponseBody
	public String getTopFive(@RequestBody final String param) {
		JsonObject jsonObject = new JsonObject();
		JsonArray fourTypeTopFivePaymentYear = paymentYearsAnalysisService.getFourTypeTopFivePaymentYearAnalysis(param);
		jsonObject.addProperty("fourTypeTopFivePaymentYear",
				fourTypeTopFivePaymentYear.toString().replace("YEARNUM", "name").replace("POLICYNO_COUNT", "value"));
		logger.info(jsonObject.toString());
		return jsonObject.toString();
	}

}
