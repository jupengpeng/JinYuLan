package com.jiudao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.service.MainRiskAnalyseService;

/**
 * 主力险种分析
 * 
 * @author qiong_c
 * 
 */
@Controller
@RequestMapping("/analyse")
public class MainRiskAnalyseController extends AbstractController {

	@Autowired
	MainRiskAnalyseService mainRiskAnalyseService;

	/**
	 * 排名前10主力险种保单件数
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/top10")
	@ResponseBody
	public String getTop10(@RequestBody String paramStr) {

		JsonObject param = new Gson().fromJson(paramStr, JsonObject.class);

		String num = param.get("insuranceNum").getAsString();
		String startTime = param.get("startTime").getAsString();
		String endTime = param.get("endTime").getAsString();
		
		String branchCode = param.get("branchCode").getAsString();

		JsonArray arr = mainRiskAnalyseService.getTop10Data(num, startTime,
				endTime, branchCode);
		logger.info(arr.toString());
		return arr.toString();
	}

	/**
	 * 主力险种数量分布
	 * @param paramStr
	 * @return
	 */
	@RequestMapping("/distribution")
	@ResponseBody
	public String getDistribution(@RequestBody String paramStr) {
		JsonObject param = new Gson().fromJson(paramStr, JsonObject.class);
		String startTime = param.get("startTime").getAsString();
		String endTime = param.get("endTime").getAsString();
		String branchCode = param.get("branchCode").getAsString();
		JsonArray arr = mainRiskAnalyseService.getMainRiskData(startTime,
				endTime, branchCode);
		return arr.toString();
	}

}
