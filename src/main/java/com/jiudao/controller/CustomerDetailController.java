package com.jiudao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.service.CustomerDetailService;  

/**
 * 
 * @author jpp
 * 客群详细分析 controller
 *
 */
@Controller
@RequestMapping("/customerDetailController")
public class CustomerDetailController {
	
	@Autowired
	private CustomerDetailService customerDetailService;
	
	/**
	 * @param param
	 * @return String
	 * 四大客群分析
	 */
	@RequestMapping("/getCustomerDetail")
	@ResponseBody
	public String getCustomerDetail(@RequestBody final String param){
		return customerDetailService.getallData(param);
	}
	/**
	 * @param param
	 * @return String
	 * 四大客群分析
	 */
	@RequestMapping("/getRiskList")
	@ResponseBody
	public String getRiskList(){
		JsonArray riskList=customerDetailService.getRiskList();
		return riskList.toString();
	}
	/**
	 * @param param
	 * @return String
	 * 公共维度下的所有分公司列表
	 */
	@RequestMapping("/getIntorgInfo")
	@ResponseBody
	public String getIntorgName(){
		JsonArray IntorgList=customerDetailService.getIntorgInfo();
		return IntorgList.toString();
	}
}
