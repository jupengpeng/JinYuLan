package com.jiudao.controller;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.service.CustomerTypeAverageAnalysisService;

/**
 * 客群件均分析controller
 * 
 * @author gyj
 *
 */
@Controller
@RequestMapping("/customerTypeAverageAnalysis")
public class CustomerTypeAverageAnalysisController extends AbstractController {

	@Autowired
	CustomerTypeAverageAnalysisService customerTypeAverageAnalysisService;

	/**
	 * 保单总数，投保人总数，总保费
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getAverage")
	@ResponseBody
	public String getAverage(@RequestBody final String param) {
		return customerTypeAverageAnalysisService.getAverage(param).toString();
	}

	/**
	 * 四大客群客均件数
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getFourTypeAveragePolicyno")
	@ResponseBody
	public String getFourTypeAveragePolicyno(@RequestBody final String param) {
		return customerTypeAverageAnalysisService.getFourTypeAveragePolicyno(param).toString();
	}

	/**
	 * 四大客群客均保费
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getFourTypeAverageMoney")
	@ResponseBody
	public String getFourTypeAverageMoney(@RequestBody final String param) {
		return customerTypeAverageAnalysisService.getFourTypeAverageMoney(param).toString();
	}

	/**
	 * 四大客群件均保额
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getFourTypeAverageMoneyForClassType")
	@ResponseBody
	public String getFourTypeAverageMoneyForClassType(@RequestBody final String param) {
		return customerTypeAverageAnalysisService.getFourTypeAverageMoneyForClassType(param).toString();
	}

	/**
	 * 客群件均分析页面
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/allCustomerTypeAverageAnalysis")
	@ResponseBody
	public String allCustomerTypeAverageAnalysis(@RequestBody final String param) {
		JsonObject jsonObject = new JsonObject();
		//保单总数，投保人总数，总保费
		JsonObject average = customerTypeAverageAnalysisService.getAverage(param);
		// 四大客群客均件数
		JsonArray fourTypeAveragePolicyno = customerTypeAverageAnalysisService.getFourTypeAveragePolicyno(param);
		//四大客群客均保费
		JsonArray fourTypeAverageMoney = customerTypeAverageAnalysisService.getFourTypeAverageMoney(param);
		//四大客群件均保额
		JsonArray fourTypeAverageMoneyForClassType = customerTypeAverageAnalysisService
				.getFourTypeAverageMoneyForClassType(param);
		double COUNT_POLICYNO = average.get("COUNT_POLICYNO").getAsDouble();
		double COUNT_PERSON = average.get("COUNT_PERSON").getAsDouble();
		double COUNT_MONEY = average.get("COUNT_MONEY").getAsDouble();
		// 保留两位小数
		jsonObject.addProperty("average_policyno", new BigDecimal(COUNT_PERSON == 0 ? 0 : COUNT_POLICYNO / COUNT_PERSON)
				.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		jsonObject.addProperty("average_money", new BigDecimal(COUNT_PERSON == 0 ? 0 : COUNT_MONEY / COUNT_PERSON)
				.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue());
		jsonObject.add("fourTypeAveragePolicyno", fourTypeAveragePolicyno);
		jsonObject.add("fourTypeAverageMoney", fourTypeAverageMoney);
		jsonObject.add("fourTypeAverageMoneyForClassType", fourTypeAverageMoneyForClassType);
		logger.info(jsonObject.toString());
		return jsonObject.toString();
	}

}
