package com.jiudao.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.service.DemoService;

@Controller
@RequestMapping("/demo")
public class DemoController extends AbstractController{
	private static final String VIEW="/demo";
	@Autowired
	DemoService demoService;
	
	@RequestMapping("/index")
	public String index(ModelMap model){
		logger.debug("/demo/index");
		model.put("name", "Chasel");
		return VIEW.concat("/index");
	}
	
	@RequestMapping("/ajax")
	@ResponseBody
	public String ajax(@RequestBody String paramStr) throws IOException {
		logger.info("param:"+paramStr);
		JsonObject param=new Gson().fromJson(paramStr, JsonObject.class);
		JsonArray a = demoService.getUser(param.get("type").getAsString());
		
		logger.info(a.toString());
		return a.toString();
	}
	@RequestMapping("/agg")
	@ResponseBody
	public String agg() throws IOException {
		JsonArray a = demoService.getAgg();
		logger.info(a.toString());
		return a.toString();
	}

}
