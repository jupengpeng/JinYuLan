package com.jiudao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.jiudao.service.EmpnoAnalysisService;
import com.jiudao.service.PaymentYearsAnalysisService;

/**
 * 营销员分析controller
 * 
 * @author gyj
 *
 */
@Controller
@RequestMapping("/empnoAnalysis")
public class EmpnoAnalysisController extends AbstractController {
	@Autowired
	EmpnoAnalysisService empnoAnalysisService;

	/**
	 * 营销员分析
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping("/getAllAnalysis")
	@ResponseBody
	public String getAllAnalysis(@RequestBody final String param) {
		//首次举绩
		JsonArray firstDays = empnoAnalysisService.getFirstDaysAnalysis(param);
		//转正
		JsonArray regularDays = empnoAnalysisService.getRegularDaysAnalysis(param);
		//首次晋升
		JsonArray promotionDays = empnoAnalysisService.getPromotionDaysAnalysis(param);
		//留存时间
		JsonArray stayDays = empnoAnalysisService.getStayDaysAnalysis(param);
		//入职三月平均fyc
		JsonArray threeMonthFyc = empnoAnalysisService.getThreeMonthFycAnalysis(param);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("firstDays",
				firstDays.toString().replace("FIRSTDAYS", "name").replace("COUNT_EMPNO", "value"));
		jsonObject.addProperty("regularDays",
				regularDays.toString().replace("REGULARDAYS", "name").replace("COUNT_EMPNO", "value"));
		jsonObject.addProperty("promotionDays",
				promotionDays.toString().replace("PROMOTIONDAYS", "name").replace("COUNT_EMPNO", "value"));
		jsonObject.addProperty("stayDays",
				stayDays.toString().replace("STAYDAYS", "name").replace("COUNT_EMPNO", "value"));
		jsonObject.addProperty("threeMonthFyc",
				threeMonthFyc.toString().replace("THREE_MONTHS_FYC", "name").replace("COUNT_EMPNO", "value"));
		return jsonObject.toString();
	}

	/**
	 * 入职后前三月FYC
	 * 
	 * @param param
	 * @param model
	 * @return
	 */
	@RequestMapping("/threeMonthFyc")
	@ResponseBody
	public String threeMonthFyc(@RequestBody final String param) {
		JsonArray threeMonthFyc = empnoAnalysisService.getThreeMonthFycAnalysis(param);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("threeMonthFyc",
				threeMonthFyc.toString().replace("THREE_MONTHS_FYC", "name").replace("COUNT_EMPNO", "value"));
		return jsonObject.toString();
	}

	/**
	 * 入职后前六月FYC
	 * 
	 * @param param
	 * @param model
	 * @return
	 */
	@RequestMapping("/sixMonthFyc")
	@ResponseBody
	public String sixMonthFyc(@RequestBody final String param) {
		JsonArray sixMonthFyc = empnoAnalysisService.getSixMonthFycAnalysis(param);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("sixMonthFyc",
				sixMonthFyc.toString().replace("SIX_MONTHS_FYC", "name").replace("COUNT_EMPNO", "value"));
		return jsonObject.toString();
	}

	/**
	 * FYC
	 * 
	 * @param param
	 * @param model
	 * @return
	 */
	@RequestMapping("/fyc")
	@ResponseBody
	public String fyc(@RequestBody final String param) {
		String fyc = empnoAnalysisService.getFycAnalysis(param);
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("fyc", fyc);
		return jsonObject.toString();
	}

}
