package com.jiudao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jiudao.service.ExcelService;
/**
 * 导出数据至excel
 * @author gyj
 *
 */


@Controller
@RequestMapping("/excel")
public class ExcelController extends AbstractController{
	
	@Autowired
	ExcelService excelService;
    
	/**
	 * 导出excel
	 * @param param
	 * @return
	 */
	@RequestMapping("/exportExcel")
	@ResponseBody
	public String exportExcel(@RequestBody final String param){
		return excelService.out(param);
	}

}
