package com.jiudao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jiudao.service.CustomerGAService;

/**
 * 
 * @author Administrator
 * 客群分析 controller
 *
 */
@Controller
@RequestMapping("/customerGAController")
public class CustomerGAController {
	
	@Autowired
	private CustomerGAService customerGAService;
	/**
	 * @param param
	 * @return String
	 * 客群分析数据查询
	 */
	@RequestMapping("/getData")
	@ResponseBody
	public String getData(@RequestBody final String param){
		return customerGAService.getData(param);
	}
}
