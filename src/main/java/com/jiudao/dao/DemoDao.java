package com.jiudao.dao;

import org.springframework.stereotype.Repository;

import com.google.gson.JsonArray;
import com.jiudao.entity.User;

@Repository
public class DemoDao extends BaseDao{
	User user=new User();
	public JsonArray getUser(String id){
		String sql="SELECT * from "+user.tablename()+" where "+user.column(User.ID)+"="+id;
		logger.info(sql);
		String columns=User.NAME+","+User.GENDER+","+User.AGE;
		return executeQuery(sql, columns);
	}

	public JsonArray getMeasureGroupByGender(){
		String age=user.column(User.AGE);
		String sql="SELECT count(*) COUNT,sum("+age+") SUM_AGE,avg("+age+") AVG_AGE,"+user.column(User.GENDER)
				+" GENDER from "+user.tablename()
				+" group by "+user.column(User.GENDER);
		logger.info(sql);
		String columns="COUNT,SUM_AGE,AVG_AGE,GENDER";
		return executeQuery(sql, columns);
	}
	
	public JsonArray getPaymentYearsAnalysis(){
		String sql="select applist.yearnum YEARNUM,count(*) COUNT from jylbase.riskcon as riskcon "
	              +"left join jylbase.applist as applist"
				  +"on riskcon.policyno = applist.policyno"
	              +"and riskcon.classcode = applist.classcode";
		logger.info(sql);
		String columns="YEARNUM,COUNT";
		return executeQuery(sql, columns);
	}
	
}
