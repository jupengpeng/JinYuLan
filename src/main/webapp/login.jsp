<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html style="height:100%;">
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8" />
	<title>中国太平洋保险（集团）股份有限公司</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta content="" name="description" />
	<meta content="" name="author" />
	<%@include file="/jsp/resource/head.jsp" %>
</head>
<body class="login-body">
    <div class="login-container">
        <div class="login-logo">
            <img src="static/image/all-slogan.png" />
            <p>中国太平洋保险（集团）股份有限公司</p>
            <p>China Pacific Insurance(Group)Co.,Ltd.</p>
        </div>
        <div class="login-content">
        	<img src="static/image/jyl.png" >
        	<img class="jyl1" src="static/image/jyl1.png" >
        	<img class="jyl2" src="static/image/jyl2.png" >
            <h3>金玉兰数据分析系统</h3>
            <div><button id="login" onclick="login()">登录</button></div>
        </div>
    </div>


<%@include file="/jsp/resource/foot.jsp" %>
<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins
        App.init();
    });
    
    function login() {
    	$.post("user/login");
    }
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->

</html>
