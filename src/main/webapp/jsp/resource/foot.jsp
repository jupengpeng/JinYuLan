<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
	<%-- <script src="${resource}plugins/jQuery/jQuery-2.1.4.min.js?rand=${rand}"></script> --%>
	<%-- <script src="${resource}plugins/Angular/angular.min.js?rand=${rand}"></script> --%>
	<script src="${resource}plugins/jQuery/jquery-1.10.1.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/jQuery/jquery-ui-1.10.1.custom.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/jQuery/jquery.slimscroll.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/angular-1.2.1/angular.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/angular-1.2.1/angular-cookies.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/angular-1.2.1/angular-route.min.js" type="text/javascript"></script>
	<script src="${resource}plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
	<script src="${resource}plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
	<script src="${resource}plugins/Echarts3/echarts.js" type="text/javascript"></script>
	<script src="${resource}app/metroApp.js" type="text/javascript"></script>
		<script src="${resource}plugins/bootstrap-datetimepicker/js/moment.js" type="text/javascript"></script>
		<script src="${resource}plugins/bootstrap-datetimepicker/js/daterangepicker.js" type="text/javascript"></script>

</body>
</html>
