<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!-- Tell the browser to be responsive to screen width -->
	<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
	<%@include file="/jsp/resource/env.jsp" %>
	<!-- Bootstrap 3.3.5 -->
  <%--   <link rel="stylesheet" href="${resource}bootstrap/css/bootstrap.css?rand=${rand}"> --%>
    <!-- Fonts -->
    <%-- <link href="${resource}plugins/font-awesome/css/font-awesome.min.css?rand=${rand}" rel="stylesheet" type="text/css">
	<link href="${resource}plugins/nivo-lightbox/nivo-lightbox.css?rand=${rand}" rel="stylesheet" />
	<link href="${resource}plugins/nivo-lightbox/nivo-lightbox-theme/default/default.css?rand=${rand}" rel="stylesheet" type="text/css" />
	<link href="${resource}plugins/animate.css?rand=${rand}" rel="stylesheet" /> --%>
	<link href="${resource}css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/style-metro.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/style.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/style-responsive.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/default.css" rel="stylesheet" type="text/css" id="style_color"/>
	<link href="${resource}css/uniform.default.css" rel="stylesheet" type="text/css"/>
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	
	
	 	<link href="${resource}css/daterangepicker.css" rel="stylesheet" type="text/css" />
	 		<!--时间控件 -->
		<!--<link href="${resource}css/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />-->
	<link href="${resource}css/fullcalendar.css" rel="stylesheet" type="text/css"/>
	<link href="${resource}css/jqvmap.css" rel="stylesheet" type="text/css" media="screen"/>
	
    <!-- Squad theme CSS -->


</head>
</html>

