<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
<%@include file="../resource/head.jsp" %>
<title>DEMO INDEX</title>
</head>
<body ng-app="myApp" ng-controller="ctrl">
	<input type="text" ng-model="id"/>
	<button ng-click="search()">search</button>
	<table>
		<thead>
			<th>NAME</th>
			<th>GENDER</th>
			<th>AGE</th>
		</thead>
		<tr ng-repeat="x in users">
			<td>{{x.NAME}}</td>
			<td>{{x.GENDER}}</td>
			<td>{{x.AGE}}</td>
		</tr>
	</table>
	
	<button ng-click="agg()">agg</button>
	<table>
		<thead>
			<th>COUNT</th>
			<th>SUM_AGE</th>
			<th>AVG_AGE</th>
			<th>GENDER</th>
		</thead>
		<tr ng-repeat="x in aggage">
			<td>{{x.COUNT}}</td>
			<td>{{x.SUM_AGE}}</td>
			<td>{{x.AVG_AGE}}</td>
			<td>{{x.GENDER}}</td>
		</tr>
	</table>
	<%@include file="../resource/foot.jsp" %>
	<script src="${resource }app/demo/demo.js"></script>
</body>
</html>
