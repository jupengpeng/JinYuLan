var app = angular.module('indexApp', [ 'ngRoute','ngCookies']);
app.controller("indexCtrl", function($scope, $http, $rootScope,$cookieStore) {
    $scope.context = context;
    $scope.startDate = "";
    $scope.endDate = "";
    //时间控件
//    $('#reservation').daterangepicker(null, function(start, end, label) {
//        console.log(start.toISOString(), end.toISOString(), label);
//      });
    $("#start-date").datetimepicker({
        language : 'zh-CN',
        weekStart : 1,
        todayBtn : 1,
        autoclose : 1,
        todayHighlight : 1,
        startView : 2,
        minView : 2,
        forceParse : 0
    }).on("changeDate change", function() {
        $("#start-date").datetimepicker("setEndDate", $("#end-date").val());
        $rootScope.startDate = $("#start-date").val();
    });
    $("#end-date").datetimepicker({
        language : 'zh-CN',
        weekStart : 1,
        todayBtn : 1,
        autoclose : 1,
        todayHighlight : 1,
        startView : 2,
        minView : 2,
        forceParse : 0,
        endDate : new Date()
    }).on("changeDate change", function() {
        $("#end-date").datetimepicker("setStartDate", $("#start-date").val());
        $rootScope.endDate = $("#end-date").val();
    });
    $('.form_date').datetimepicker({
        language : 'fr',
        weekStart : 1,
        todayBtn : 1,
        autoclose : 1,
        todayHighlight : 1,
        startView : 2,
        minView : 2,
        forceParse : 0
    });

	if (typeof $rootScope.startDate == "undefined"
			|| $rootScope.startDate == "") {
		$rootScope.startDate = "2011-01-01";
	}
	if (typeof $rootScope.endDate == "undefined" || $rootScope.endDate == "") {
		$rootScope.endDate = "2017-10-31";
	}

	// 选择的分公司名字
	// $scope.intorg_name="";

	// 选择的分公司代码
	$rootScope.branchCode = "";

	// 分公司列表
	$scope.intorg_list;
	// 获取所有分公司列表（包括分公司名称和代码）
	$scope.intorgList = function() {
		$http({
			method : 'post',
			url : context + '/customerDetailController/getIntorgInfo'
		}).success(function(req) {
			$scope.intorg_list = req;
			console.log("分公司", $scope.intorg_list);
		});
	};
	$scope.intorgList();
	//
	$scope.changeIntorg = function() {
		// $scope.intorg_name=x.intorgname;
		console.log("branchCode", $scope.branchCode);
		if ($scope.branchCode == null) {
			$rootScope.branchCode = '';
		} else {
			$rootScope.branchCode = $scope.branchCode;
		}
	};

	// 导出excel
	$scope.exportExcel = function() {
		console.log("导出excel", $rootScope.excelData);
		$http({
			method : 'post',
			url : context + '/excel/exportExcel',
			data : $rootScope.excelData
		}).success(function(data) {
			console.log("downLoadPath", data);
			window.open("/jylExcel/" + data);
		});
	};
});

// 路由设置
app.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'static/tpl/types.html',
		controller : 'typesCtrl'
	}).when('/types', {
		templateUrl : 'static/tpl/types.html',
		controller : 'typesCtrl'
	}).when('/customers', {
		templateUrl : 'static/tpl/customers.html',
		controller : 'customersCtrl'
	}).when('/payment', {
		templateUrl : 'static/tpl/payment.html',
		controller : 'paymentCtrl'
	}).when('/average', {
		templateUrl : 'static/tpl/average.html',
		controller : 'averageCtrl'
	}).when('/clients', {
		templateUrl : 'static/tpl/clients.html',
		controller : 'clientsCtrl'
	}).when('/salesman', {
		templateUrl : 'static/tpl/salesman.html',
		controller : 'salesmanCtrl'
	}).otherwise({
		redirectTo : '/'
	});
} ]);

// 路由监听
app.run(function($rootScope, $location) {
	console.log("enter...");
	$rootScope.$on("$routeChangeStart", function(event, next, current) {
		var currentPath = $location.path();
		$rootScope.currentPath = currentPath;
		console.log(currentPath);
	});
});
