//分类大数据分析_险种分析
app.controller("typesCtrl", function($scope,$rootScope, $http) {
	$scope.context=context;
	var myChart0 = echarts.init(document.getElementById('types-top-left'));
	var myChart1 = echarts.init(document.getElementById('types-top-right'));
	var myChart2 = echarts.init(document.getElementById('types-bottom'));
	
	$scope.top10 = [];
	// 默认保单件数
	$scope.insuranceNum = 1;
	
	$rootScope.excelData = {};
	/**
	 * 前十名险种保单件数
	 */
	$scope.searchTop10 = function() {
		console.log("start-",$rootScope.startDate);
		console.log("end-",$rootScope.endDate);
		$http({
			url : context + '/analyse/top10',
			data : {
				insuranceNum : $scope.insuranceNum,
				branchCode : $rootScope.branchCode,
				endTime : $rootScope.endDate,
				startTime : $rootScope.startDate
			},
			method : 'post'
		}).success(function(data) {
			$rootScope.excelData.mainRiskTop10 = data;
			console.log('data', data);
			$scope.top10 = data;
			myChart1.setOption({
				backgroundColor : '#fff',
				tooltip : {
					trigger : 'axis',
					padding : [ 5, 10 ],
					formatter : "{b} :  {c} ",
					extraCssText : 'border-radius: 5px',
					axisPointer : {
						type : 'shadow',
						shadowStyle : {
							opacity : 0
						}
					}
				},
				toolbox : {
					show : true,
					feature : {
						saveAsImage : {
							show : true
						}
					},
					bottom : 10,
					right : 0
				},
				grid : {
					left : '3%',
					right : '5%',
					bottom : '3%',
					top : '3%',
					containLabel : true
				},
				xAxis : {
					type : 'value',
					boundaryGap : [ 0, 0.01 ],
					splitLine : {
						show : false
					},

					axisLine : {
						lineStyle : {
							color : '#9D9D9D'
						}
					}
				},
				yAxis : {
					type : 'category',
					axisLine : {
						lineStyle : {
							color : '#9D9D9D'
						}
					},
					data : function() {
						var list = [];
						for (var i = 0; i < $scope.top10.length; i++) {
							list.push($scope.top10[i]['CLASSNAME']);
						}
						return list;
					}(),
				},
				series : [{
					type : 'bar',
					itemStyle : {
						normal : {
							color : '#6ca8ee',
							opacity : '0.8'
						},
						emphasis : {
							color : '#2493ca'
						}
					},
					barWidth : '50%',
					data : function() {
						var list = [];
						for (var i = 0; i < $scope.top10.length; i++) {
							list.push($scope.top10[i]['NUM']);
						}
						return list;
					}(),
				} ]
			});
		});
	};
	$scope.searchTop10();
	$scope.choseNum = function() {
		$scope.searchTop10();
	};
	
	//时间控件联动
	$("#end-date").on("change",function(){
		$scope.searchTop10();
		$scope.searchMainRisk();
	});
	$("#start-date").on("change",function(){
		$scope.searchTop10();
		$scope.searchMainRisk();
	});
	//选择分公司联动
	$("#branch").on("change",function(){
		console.log("changeBranch...");
		$scope.searchTop10();
		$scope.searchMainRisk();
	});
	
	// 占比2%以上主力险种分布 及件均保p1/p3
	$scope.searchMainRisk = function() {
		$http({
			url : context + '/analyse/distribution',
			data : {
				branchCode : $rootScope.branchCode,
				endTime : $rootScope.endDate,
				startTime : $rootScope.startDate
			},
			method : 'post'
		}).success(function(data) {
			console.log('data',data);
			$rootScope.excelData.mainRiskDistribution = data;
			$scope.riskData=[];
			$scope.mainInsurances=[];
			$scope.mainDeals=[];
			$scope.mainFees=[];
			for (var i=0; i<data.length;i++) {
				//p1
				$scope.riskData.push({name:data[i]['CLASSNAME'],value:data[i]['CNT']});
				//p3
				$scope.mainInsurances.push(data[i]['CLASSNAME']);
				$scope.mainDeals.push(data[i]['CNT']);
				$scope.mainFees.push(parseFloat(data[i]['AVERAGE']).toFixed(2));
			}
			console.log('riskData',$scope.riskData);
			console.log('保单件数', $scope.mainDeals);
			console.log('件均保费', $scope.mainFees);
			var maxDeal = Math.max.apply(null, $scope.mainDeals);
			var maxFee = Math.max.apply(null, $scope.mainFees);
			var axisDeal = maxDeal.toString().length;
			var axisFee = maxFee.toString().length;
			var a = parseInt(maxDeal/(Math.pow(10,axisDeal-1)));
			var b = (a+1)*(Math.pow(10,axisDeal-1));
			var c = parseInt(maxFee/(Math.pow(10,axisFee-1)));
			var d = (c+1)*(Math.pow(10,axisFee-1));
			console.log(d);
			//主力险种占比图p1
			myChart0.setOption({
				backgroundColor : '#fff',
				tooltip : {
					show: false,
					trigger : 'item',
					formatter : "{a} <br/>{b}: {c} ({d}%)"
				},
				toolbox : {
					show : true,
					feature : {
						saveAsImage : {
							show : true
						}
					},
					bottom : 10,
					right : 0
				},
				series : [ {
					type : 'pie',
					radius : [ '40%', '70%' ],
					avoidLabelOverlap : false,
					label : {
						normal : {
							show : true,
							textStyle : {
								color: '#737373'
							},
							position : 'outside',
							formatter : "{b} \n{c} \n({d}%)"
						}
					},
					labelLine: {
	                	normal:{
	                		lineStyle:{
	                			color: '#D0D0D0'
	                		}
	                	}
	                },
	                color:['#d32f2f','#ad1457','#6a1b9a','#512da8','#283593','#1976d2','#0097a7',
			               '#00796b','#388e3c','#689f38','#c0ca33','#fbc02d','#f57c00','#e64a19'],
					data : $scope.riskData
				},
				{
					type : 'pie',
					radius : [ '40%', '70%' ],
					avoidLabelOverlap : false,
					label : {
						normal : {
							show : false,
							position : 'center',
							formatter : "{b} \n{c} \n({d}%)"
						},
						emphasis : {
							show : true,
							textStyle : {
								color: '#324e66',
								fontSize: 13
							},
							lineHeight: 26,
							formatter : "{b|{d}%} \n保单件数 {c}",
							rich:{
								b:{
									color: '#035eae',
									fontSize: 20
								}
							}
						}
					},
					color:['#d32f2f','#ad1457','#6a1b9a','#512da8','#283593','#1976d2','#0097a7',
			               '#00796b','#388e3c','#689f38','#c0ca33','#fbc02d','#f57c00','#e64a19'],
					data : $scope.riskData
				}]
			});
			// 主力险种件均保费分布p3
			myChart2.setOption({
				tooltip : {
					trigger : 'axis',
					padding : [ 5, 10 ],
					extraCssText : 'border-radius: 5px',
					axisPointer : {
						type : 'shadow',
						shadowStyle : {
							opacity : 0
						}
					}
				},
				toolbox : {
					show : true,
					feature : {
						saveAsImage : {
							show : true
						}
					},
					bottom : 10,
					right : 0
				},
				legend : {
					data : [ '件均保费', '保单件数' ]
				},
				xAxis : [ {
					type : 'category',
					data : $scope.mainInsurances,
					axisPointer : {
						type : 'shadow'
					},
					axisLine : {
						lineStyle : {
							color : '#9D9D9D'
						}
					},
					axisLabel:{
						show : true,
						interval :0,
						rotate : 11
					}
				} ],
				yAxis : [ {
					type : 'value',
					name : '件均保费',
					min : 0,
					max : d,
					interval : d/5,
//					axisLabel : {
//						formatter : '{value} 千元'
//					},
					axisLine : {
						lineStyle : {
							color : '#9D9D9D'
						}
					},
					label : {
						normal : {
							show : true,
							position : 'top'
						}
					}
				}, {
					type : 'value',
					name : '保单件数',
					min : 0,
					max : b,
					interval : b/5,
//					axisLabel : {
//						formatter : '{value} 万'
//					},
					axisLine : {
						lineStyle : {
							color : '#9D9D9D'
						}
					},
					label : {
						normal : {
							show : true,
							position : 'top'
						}
					}
				} ],
				series : [ {
					name : '件均保费',
					type : 'bar',
					barWidth : '30%',
					itemStyle : {
						normal : {
							color : '#f5e36b'
						}
					},
					label : {
						normal : {
							show : true,
							position : 'top',
							textStyle:{
								fontSize: 14,
								color: '#6B6B6B'
							}
						}
					},
					data : $scope.mainFees
				}, {
					name : '保单件数',
					type : 'bar',
					barWidth : '30%',
					itemStyle : {
						normal : {
							color : '#2493ca'
						}
					},
					label : {
						normal : {
							show : true,
							position : 'top',
							textStyle:{
								fontSize: 14,
								color: '#6B6B6B'
							}
						}
					},
					yAxisIndex : 1,
					data : $scope.mainDeals
				} ]
			});
		});
	};
	$scope.searchMainRisk();
	
	$(window).resize(function() {
		setTimeout(function() {
			myChart0.resize();
			myChart1.resize();
			myChart2.resize();
		}, 100);
	});
});
