//客群详细分析
app.controller("clientsCtrl",function($scope, $http,$rootScope,$cookieStore) {
			$scope.context = context;
			$scope.cusDetail_ygqn = 0;// 阳光青年
			$scope.cusDetail_jycj = 0;// 精英才俊
			$scope.cusDetail_xkzj = 0;// 小康之家
			$scope.cusDetail_jdyd = 0;// 家大业大
			$scope.cusDetail_qt = 0;// 其他
			$scope.apidnum;// 投保人数
			$scope.pidnum;// 被投保人
			$scope.longrisk;// 长险件数
			$scope.shortrisk;// 短险件数
			$scope.oldPerson;// 老客户数
			$scope.man;// 男
			$scope.woman;// 女
			$scope.risk_list=[];// 所有险种
			$scope.riskMark=[];// 
			$scope.sexNum=[];//性别分布
			$scope.risk_choose = "";// 选择的险种
			$rootScope.excelData = {};
			$scope.classcode="";
			var cookies = $cookieStore.get("history"); 
			$('.js-example-basic-single').select2();
			// 获取所有险种列表
			$scope.riskList = function() {
				$http({
						method : 'post',
						url : context
								+ '/customerDetailController/getRiskList'
					}).success(function(req) {
						$scope.risk_list = req;
						if(typeof cookies != "undefined" && cookies !=""  ){
							//删除risk_list中已存在于cookie内的数据
							for(var i=0;i<cookies.length;i++) {
								for (var j=0;j < $scope.risk_list.length; j++) {
									if(typeof cookies[i] != "undefined" && cookies[i] !=null){
										if(cookies[i]["classcode"] == $scope.risk_list[j]["classcode"]) {
											$scope.risk_list.splice(j,1);
											break;
										}
									}
								}
							}
							//将cookie历史记录 加入险种列表
							for(var i=0; i<cookies.length;i++){
								$scope.risk_list.unshift(cookies[i]);
							}
						}
				});
			};
			$scope.riskList();
			var myChart40 = echarts.init(document.getElementById('clients-right'));
			var option40 = {
				backgroundColor : '#fff',
				tooltip : {
					trigger : 'item',
					formatter : "{b}: {c} ({d}%)"
				},
				toolbox : {
					show : true,
					feature : {
						saveAsImage : {
							show : true
						}
					},
					bottom : 10,
					right : 0
				},
				calculable : true,
				series : [ {
					type : 'pie',
					radius : [ '15%', '70%' ],
					roseType : 'radius',
					minAngle : 10,
					label : {
						normal : {
							show : true,
							position : 'outside',
							textStyle : {
								color : '#737373',
								fontSize : 13
							},
							formatter : "{b|{b}}: \n{c} \n({d}%)",
							rich : {
								b : {
									color : '#324e66',
									fontSize : 13,
									lineHeight : 20
								}
							}
						},
						emphasis : {
							show : true,
							textStyle : {
							// fontSize: '30',
							// fontWeight: 'bold'
							},
							position : 'center'
						}
					},
					labelLine : {
						normal : {
							lineStyle : {
								color : '#D0D0D0'
							}
						}
					},
					itemStyle : {
						normal : {
							color : function(params) {
								var colorList = [ '#1e4b71', '#d04840',
										'#e69c40', '#e9c647', '#3486bb' ];
								return colorList[params.dataIndex];
							}
						}
					},
					data : $scope.clients
				} ]
			};

			$scope.queryDate = function() {
//				console.log("classcode",$scope.risk_choose);
				$http({		method : 'post',
							url : context
									+ '/customerDetailController/getCustomerDetail',
							data : {
								risk_choose : $scope.risk_choose,
								startDate : $rootScope.startDate,
								endDate : $rootScope.endDate,
								branchCode : $rootScope.branchCode
							}
						}).success(function(req) {
									$rootScope.excelData = req;
									$scope.cusDetail_ygqn = 0;// 阳光青年
									$scope.cusDetail_jycj = 0;// 精英才俊
									$scope.cusDetail_xkzj = 0;// 小康之家
									$scope.cusDetail_jdyd = 0;// 家大业大
									$scope.cusDetail_qt = 0;// 其他
									$scope.apidnum = 0;// 投保人数
									$scope.pidnum = 0;// 被投保人
									$scope.longrisk = 0;// 长险件数
									$scope.shortrisk = 0;// 短险件数
									$scope.oldPerson = 0;// 老客户数
									$scope.man = 0;// 男
									$scope.woman = 0;// 女
									$scope.apidnum=req.apidNum;
									$scope.pidnum = req.pidNum;
									$scope.oldPerson = req.oldNum;// 老客户数
									// 长短险
									for (var i = 0; i < req.riskMark.length; i++) {
										switch (req.riskMark[i].risk_mark) {
										case "长险":
											$scope.longrisk = req.riskMark[i].num;
											break;
										case "短险":
											$scope.shortrisk = req.riskMark[i].num;
											break;
										}
									}
									// 男女分布
									for (var i = 0; i < req.sexNum.length; i++) {
										switch (req.sexNum[i].sex) {
										case "1":
											$scope.man = req.sexNum[i].num;
											break;
										case "2":
											$scope.woman = req.sexNum[i].num;
											break;
										}
									}
									$scope.manbf = isNaN(Math
											.round($scope.man
													/ (Number($scope.man) + Number($scope.woman))
													* 100)) ? 0
											: Math
													.round($scope.man
															/ (Number($scope.man) + Number($scope.woman))
															* 100);
									$scope.womanbf = isNaN(Math
											.round($scope.woman
													/ (Number($scope.man) + Number($scope.woman))
													* 100)) ? 0
											: Math
													.round($scope.woman
															/ (Number($scope.man) + Number($scope.woman))
															* 100);
									// 四大客群
									for (var i = 0; i < req.cusDetail.length; i++) {
										switch (req.cusDetail[i].person_mark) {
										case "阳光青年":
											$scope.cusDetail_ygqn = req.cusDetail[i].num;
											break;
										case "精英才俊":
											$scope.cusDetail_jycj = req.cusDetail[i].num;
											break;
										case "小康之家":
											$scope.cusDetail_xkzj = req.cusDetail[i].num;
											break;
										case "家大业大":
											$scope.cusDetail_jdyd = req.cusDetail[i].num;
											break;
										default:
											$scope.cusDetail_qt = req.cusDetail[i].num;
										}
									}
									$scope.clients = [ {
										name : '阳光青年',
										value : $scope.cusDetail_ygqn
									}, {
										name : '精英才俊',
										value : $scope.cusDetail_jycj
									}, {
										name : '家大业大',
										value : $scope.cusDetail_jdyd
									}, {
										name : '小康之家',
										value : $scope.cusDetail_xkzj
									}, {
										name : '其他',
										value : $scope.cusDetail_qt
									} ];
									option40.series[0].data = $scope.clients;
									myChart40.setOption(option40);
								});
			};
			$scope.queryDate();
			$scope.click = function() {
//				$scope.$apply();
				console.log("risk_list",$scope.risk_list);
			};
			//选择险种
			$scope.change = function() {
				$scope.classname ="";
				if($scope.classcode == null || $scope.classcode == "") {
					$scope.risk_choose = "";
				}else{
					$scope.risk_choose = $scope.classcode;
					var temp =[];
					//遍历险种列表，搜索与classcode对应的classname
					for(var i=0;i<$scope.risk_list.length; i++) {
						if($scope.risk_list[i]["classcode"] == $scope.classcode) {
							$scope.classname = $scope.risk_list[i]["classname"];
							temp = $scope.risk_list.splice(i,1);
							console.log("--i--",temp);
							//将当前选择的险种从险种列表中删除
							$scope.risk_list.unshift(temp[0]);
							$scope.risk_list[0].attr="1";
							break;
						}
					}
					//更新cookie操作
					console.log("cookies1",cookies);
					if(typeof cookies != "undefined" && cookies !="" ) {
						//如果 cookie不为空
						var flag = false;
						for(var i=0; i<cookies.length; i++ ){
							if(typeof cookies[i] != "undefined" && cookies[i] !=null){
								if(cookies[i]["classcode"] == $scope.classcode) {
									//若cookie中已存在所选的险种，将当前险种移到数组最首位
									cookies.push(cookies.splice(i,1)[0]);
									flag = true;
									break;
								}
							}
						}
						if(flag == false) {
							//若cookie中不存在所选的险种，则在cookie中加入
							cookies.push(temp[0]);
						}
					}else {//cookie为空
						cookies = [];
						cookies.push(temp[0]);
					}
					//classname,classcode存入cookie
					$cookieStore.put("history",cookies);
				}
				$scope.queryDate();
			};
			//时间控件联动
			$("#end-date").on("change",function(){
				$scope.queryDate();
			});
			$("#start-date").on("change",function(){
				$scope.queryDate();
			});
			//选择分公司联动
			$("#branch").on("change",function(){
				console.log("changeBranch...");
				$scope.queryDate();
			});
			$(window).resize(function() {
				setTimeout(function() {
					myChart40.resize();
				}, 100);
			});
		});