//营销员分析
app.controller("salesmanCtrl", function ($scope,$http,$rootScope) {
	$scope.context = context;
	//筛选条件
	$scope.reqData={};
	$scope.reqData.beginDate=$rootScope.startDate;
	$scope.reqData.endDate=$rootScope.endDate;
	$scope.reqData.branch=$rootScope.branchCode;
	$scope.reqData.month='3';
	$scope.reqData.service='outJob'; 
	
	//监听分公司下拉选
	$scope.$watch('branchCode', function() {
		console.log('branchCode', $scope.branchCode);
		if($scope.branchCode==null){
			$scope.reqData.branch='';
		}else{
			$scope.reqData.branch=$scope.branchCode;
		}
		$scope.startData();
	});
	
	//时间控件起始时间
	$("#start-date").on("change",function(){
		$scope.reqData.beginDate=$rootScope.startDate;
		$scope.startData();
	});
	//时间控件结束时间
	$("#end-date").on("change",function(){
		$scope.reqData.endDate=$rootScope.endDate;
		$scope.startData();
	});
  
    /*
     * 入职后第一次举绩所需时间
     * entryPeople: 元数据
     * entryP:人数
     * entryDay:天数
     * entryAxisLabel :x轴上显示的数据值
     * */

    var entryPeople = [
    	{	name:'1'	,	value:1500	}	,
    	{	name:'2'	,	value:1501	}
];

    //修改数据格式
    $scope.changeData50 = function(data){
        var entryP = [];
        var entryDay =[];
        var entryAxisLabel=[]; 
     //补全缺损的数据 将没有的数据
        for(var ii=0;ii<91;ii++){
        for(var i=0;i<data.length;i++){
        	  var name = data[i].name;
        	if(name== ii ){
        	entryP.push(data[i].value);
        	entryDay.push(data[i].name);
        }else if(name>90 ){
        	entryP.push(data[i].value);
        	entryDay.push("以上");
        }
        	else{
        	entryP.push(0);
        	entryDay.push(ii); 
        		}
        	  }
        	}
        

        
        var entryAxisLabel=parseInt(i/10);  
    	    //echart划分线 
        var entryPer=Math.max.apply(Math, entryP); 
        var entryPerMax1 = entryPer/4
        var entryPerMax2 = entryPer*2/4;
        var entryPerMax3 = entryPer*3/4;

    	    $scope.option50 = {
    	            title: {

    	            },
    	            tooltip: {
    	                trigger: 'axis',
    	                padding: [5,10],
    	                formatter: "{b} :  {c} ",
    	                extraCssText: 'border-radius: 5px',
    	                axisPointer:{
    	                    type: 'shadow',
    	                    shadowStyle:{
    	                        opacity:0
    	                    }
    	                }
    	            },
    	            legend: {

    	            },
    	            grid: {
    	                left: '3%',
    	                right: '12%',
    	                bottom: '4%',
    	                containLabel: true
    	            },
    	            toolbox: {
    	                feature: {
    	                    saveAsImage: {}
    	                }
    	            },
    	            xAxis: {
//    	                min: 0,
//    	                max: 90,
//    	                type: 'value',
    	                name : '(天)',
    	                axisTick:{
    	                    show:false
    	                },
    	                splitLine:{
    	                	 show:false
    	                },
    	                axisLine:{
    	                    lineStyle:{
    	                        color:'#9D9D9D'
    	                    }
    	                },
    	                type: 'category',
    	                boundaryGap: false,
    	                axisLabel:{			// echarts 中X轴上显示的数据
    	                    interval:59
    	                },
    	                data: entryDay
    	            },
    	            yAxis : [
    	                {
    	                	splitLine: {
    	                        show: false
    	                    },
    	                    name : '(人)',
    	                    axisTick:{
    	                        show:false
    	                    },
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    },
//    	                    axisLabel: {
//    	                        formatter: '{value}万 '
//    	                    },
    	                }
    	            ],

    	            visualMap: {

    	                top: 1110,
    	                right: 1110,
    	                pieces: [{
    	                    gt: 0,
    	                    lte: entryPerMax1,
    	                    color: '#069d6b'
    	                }, {
    	                    gt: entryPerMax1,
    	                    lte: entryPerMax2,
    	                    color: '#ffe455'
    	                }, {
    	                    gt: entryPerMax2,
    	                    lte: entryPerMax3,
    	                    color: '#ffa245'
    	                }],
    	                outOfRange: {
    	                    color: '#cc0033'
    	                }
    	            },
    	            series: [
    	                {
    	                    name:'人',
    	                    type:'line',


    	                    data:entryP,

    	                    markLine: {
    	                        symbolSize:Array,
    	                        label:{
    	                            emphasis:{
    	                                show:  false ,

    	                            },
    	                        },
    	                        lineStyle:{
    	                            normal :{
    	                                type:'soild',
    	                                width:1,
    	                            },
    	                        },
    	                        silent: true,
    	                        data: [{
    	                            yAxis: entryPerMax1
    	                        }, {
    	                            yAxis: entryPerMax2
    	                        }, {
    	                            yAxis: entryPerMax3
    	                        },]
    	                    },
    	                },
    	            ]
    	        };
    };

    var myChart50 = echarts.init(document.getElementById('salesman-top-left'));
    //myChart50.setOption(option50);

/*
 * 
 * 入职后转正所需时间
 * positiveP:人数
 * positiveT:时间
 * */
    
    var pdata =[
    	{	name:'1'	,	value:1500	}	,
    	{	name:'2'	,	value:1501	}	,
    	{	name:'3'	,	value:1502	}	,
    	{	name:'4'	,	value:1503	}	,
    	{	name:'5'	,	value:1504	}	,
    	{	name:'6'	,	value:1505	}	,
    	{	name:'7'	,	value:1506	}	,
    	{	name:'8'	,	value:1507	}	,
    	{	name:'9'	,	value:1508	}	,
    	{	name:'10'	,	value:1509	}	,
    	{	name:'11'	,	value:1510	}	,
    	{	name:'12'	,	value:1511	}	,
    	{	name:'13'	,	value:1512	}	,
    	{	name:'14'	,	value:1513	}	,
    	{	name:'15'	,	value:1514	}	,
    	{	name:'16'	,	value:1515	}	,
    	{	name:'17'	,	value:1516	}	,
    	{	name:'18'	,	value:1517	}	,
    	{	name:'19'	,	value:1518	}	,
    	{	name:'20'	,	value:1519	}	,
    	{	name:'21'	,	value:1520	}	,
    	{	name:'22'	,	value:1521	}	,
    	{	name:'23'	,	value:1522	}	,
    	{	name:'24'	,	value:1523	}	,
    	{	name:'25'	,	value:1524	}	,
    	{	name:'26'	,	value:1525	}	,
    	{	name:'27'	,	value:1526	}	,
    	{	name:'28'	,	value:1527	}	,
    	{	name:'29'	,	value:1528	}	,
    	{	name:'30'	,	value:1529	}	,
    	{	name:'31'	,	value:15300	}	,
    	{	name:'32'	,	value:1531	}	,
    	{	name:'33'	,	value:1532	}	,
    	{	name:'34'	,	value:1533	}	,
    	{	name:'35'	,	value:1534	}	,
    	{	name:'36'	,	value:1535	}	,
    	{	name:'37'	,	value:1536	}	,
    	{	name:'38'	,	value:1537	}	,
    	{	name:'39'	,	value:1538	}	,
    	{	name:'40'	,	value:1539	}	,
    	{	name:'41'	,	value:1540	}	,
    	{	name:'42'	,	value:1541	}	,
    	{	name:'43'	,	value:1542	}	,
    	{	name:'44'	,	value:1543	}	,
    	{	name:'45'	,	value:1544	}	,
    	{	name:'46'	,	value:1545	}	,
    	{	name:'47'	,	value:1546	}	,
    	{	name:'48'	,	value:1547	}	,
    	{	name:'49'	,	value:1548	}	,
    	{	name:'50'	,	value:1549	}	,
    	{	name:'51'	,	value:1550	}	,
    	{	name:'52'	,	value:1551	}	,
    	{	name:'53'	,	value:1552	}	,
    	{	name:'54'	,	value:1553	}	,
    	{	name:'55'	,	value:1554	}	,
    	{	name:'56'	,	value:1555	}	,
    	{	name:'57'	,	value:1556	}	,
    	{	name:'58'	,	value:1557	}	,
    	{	name:'59'	,	value:1558	}	,
    	{	name:'60'	,	value:1559	}	,
    	{	name:'61'	,	value:1560	}	,
    	{	name:'62'	,	value:1561	}	,
    	{	name:'63'	,	value:1562	}	,
    	{	name:'64'	,	value:1563	}	,
    	{	name:'65'	,	value:1564	}	,
    	{	name:'66'	,	value:1565	}	,
    	{	name:'67'	,	value:1566	}	,
    	{	name:'68'	,	value:1567	}	,
    	{	name:'69'	,	value:1568	}	,
    	{	name:'70'	,	value:1569	}	,
    	{	name:'71'	,	value:1570	}	,
    	{	name:'72'	,	value:1571	}	,
    	{	name:'73'	,	value:1572	}	,
    	{	name:'74'	,	value:1573	}	,
    	{	name:'75'	,	value:1574	}	,
    	{	name:'76'	,	value:1575	}	,
    	{	name:'77'	,	value:1576	}	,
    	{	name:'78'	,	value:1577	}	,
    	{	name:'79'	,	value:1578	}	,
    	{	name:'80'	,	value:1579	}	,
    	{	name:'81'	,	value:1580	}	,
    	{	name:'82'	,	value:1581	}	,
    	{	name:'83'	,	value:1582	}	,
    	{	name:'84'	,	value:1583	}	,
    	{	name:'85'	,	value:1584	}	,
    	{	name:'86'	,	value:1585	}	,
    	{	name:'87'	,	value:1586	}	,
    	{	name:'88'	,	value:1587	}	,
    	{	name:'89'	,	value:1588	}	,
    	{	name:'90'	,	value:1589	}	,
    	{	name:'91'	,	value:1590	}	,
    	{	name:'92'	,	value:1591	}	,
    	{	name:'93'	,	value:1592	}	,
    	{	name:'94'	,	value:1593	}	,
    	{	name:'95'	,	value:1594	}	,
    	{	name:'96'	,	value:1595	}	,
    	{	name:'97'	,	value:1596	}	,
    	{	name:'98'	,	value:1597	}	,
    	{	name:'99'	,	value:1598	}	,
    	{	name:'100'	,	value:1599	}	,
    	{	name:'101'	,	value:1600	}	,
    	{	name:'102'	,	value:1601	}	,
    	{	name:'103'	,	value:1602	}	,
    	{	name:'104'	,	value:1603	}	,
    	{	name:'105'	,	value:1604	}	,
    	{	name:'106'	,	value:1605	}	,
    	{	name:'107'	,	value:1606	}	,
    	{	name:'108'	,	value:1607	}	,
    	{	name:'109'	,	value:1608	}	,
    	{	name:'110'	,	value:1609	}	,
    	{	name:'111'	,	value:1610	}	,
    	{	name:'112'	,	value:1611	}	,
    	{	name:'113'	,	value:1612	}	,
    	{	name:'114'	,	value:1613	}	,
    	{	name:'115'	,	value:1614	}	,
    	{	name:'116'	,	value:1615	}	,
    	{	name:'117'	,	value:1616	}	,
    	{	name:'118'	,	value:1617	}	,
    	{	name:'119'	,	value:1618	}	,
    	{	name:'120'	,	value:1619	}	,
    	{	name:'121'	,	value:1620	}	,
    	{	name:'122'	,	value:1621	}	,
    	{	name:'123'	,	value:1622	}	,
    	{	name:'124'	,	value:1623	}	,
    	{	name:'125'	,	value:1624	}	,
    	{	name:'126'	,	value:1625	}	,
    	{	name:'127'	,	value:1626	}	,
    	{	name:'128'	,	value:1627	}	,
    	{	name:'129'	,	value:1628	}	,
    	{	name:'130'	,	value:1629	}	,
    	{	name:'131'	,	value:1630	}	,
    	{	name:'132'	,	value:1631	}	,
    	{	name:'133'	,	value:1632	}	,
    	{	name:'134'	,	value:1633	}	,
    	{	name:'135'	,	value:1634	}	,
    	{	name:'136'	,	value:1635	}	,
    	{	name:'137'	,	value:1636	}	,
    	{	name:'138'	,	value:1637	}	,
    	{	name:'139'	,	value:1638	}	,
    	{	name:'140'	,	value:1639	}	,
    	{	name:'141'	,	value:1640	}	,
    	{	name:'142'	,	value:1641	}	,
    	{	name:'143'	,	value:1642	}	,
    	{	name:'144'	,	value:1643	}	,
    	{	name:'145'	,	value:1644	}	,
    	{	name:'146'	,	value:1645	}	,
    	{	name:'147'	,	value:1646	}	,
    	{	name:'148'	,	value:1647	}	,
    	{	name:'149'	,	value:1648	}	,
    	{	name:'150'	,	value:1649	}	,
    	{	name:'151'	,	value:1650	}	,
    	{	name:'152'	,	value:1651	}	,
    	{	name:'153'	,	value:1652	}	,
    	{	name:'154'	,	value:1653	}	,
    	{	name:'155'	,	value:1654	}	,
    	{	name:'156'	,	value:1655	}	,
    	{	name:'157'	,	value:1656	}	,
    	{	name:'158'	,	value:1657	}	,
    	{	name:'159'	,	value:1658	}	,
    	{	name:'160'	,	value:1659	}	,
    	{	name:'161'	,	value:1660	}	,
    	{	name:'162'	,	value:1661	}	,
    	{	name:'163'	,	value:1662	}	,
    	{	name:'164'	,	value:1663	}	,
    	{	name:'165'	,	value:1664	}	,
    	{	name:'166'	,	value:1665	}	,
    	{	name:'167'	,	value:1666	}	,
    	{	name:'168'	,	value:1667	}	,
    	{	name:'169'	,	value:1668	}	,
    	{	name:'170'	,	value:1669	}	,
    	{	name:'171'	,	value:1670	}	,
    	{	name:'172'	,	value:1671	}	,
    	{	name:'173'	,	value:1672	}	,
    	{	name:'174'	,	value:1673	}	,
    	{	name:'175'	,	value:1674	}	,
    	{	name:'176'	,	value:1675	}	,
    	{	name:'177'	,	value:1676	}	,
    	{	name:'178'	,	value:1677	}	,
    	{	name:'179'	,	value:1678	}	,
    	{	name:'180'	,	value:1679	}	,
    	{	name:'181'	,	value:1680	}	,
    	{	name:'182'	,	value:1681	}	,
    	{	name:'183'	,	value:1682	}	,
    	{	name:'184'	,	value:1683	}	,
    	{	name:'185'	,	value:1684	}	,
    	{	name:'186'	,	value:1685	}	,
    	{	name:'187'	,	value:1686	}	,
    	{	name:'188'	,	value:1687	}	,
    	{	name:'189'	,	value:1688	}	,
    	{	name:'190'	,	value:1689	}	,
    	{	name:'191'	,	value:1690	}	,
    	{	name:'192'	,	value:1691	}	,
    	{	name:'193'	,	value:1692	}	,
    	{	name:'194'	,	value:1693	}	,
    	{	name:'195'	,	value:1694	}	,
    	{	name:'196'	,	value:1695	}	,
    	{	name:'197'	,	value:1696	}	,
    	{	name:'198'	,	value:1697	}	,
    	{	name:'199'	,	value:1698	}	,
    	{	name:'200'	,	value:1699	}	,
    	{	name:'201'	,	value:1700	}	,
    	{	name:'202'	,	value:1701	}	,
    	{	name:'203'	,	value:1702	}	,
    	{	name:'204'	,	value:1703	}	,
    	{	name:'205'	,	value:1704	}	,
    	{	name:'206'	,	value:1705	}	,
    	{	name:'207'	,	value:1706	}	,
    	{	name:'208'	,	value:1707	}	,
    	{	name:'209'	,	value:1708	}	,
    	{	name:'210'	,	value:1709	}	,
    	{	name:'211'	,	value:1710	}	,
    	{	name:'212'	,	value:1711	}	,
    	{	name:'213'	,	value:1712	}	,
    	{	name:'214'	,	value:1713	}	,
    	{	name:'215'	,	value:1714	}	,
    	{	name:'216'	,	value:1715	}	,
    	{	name:'217'	,	value:1716	}	,
    	{	name:'218'	,	value:1717	}	,
    	{	name:'219'	,	value:1718	}	,
    	{	name:'220'	,	value:1719	}	,
    	{	name:'221'	,	value:1720	}	,
    	{	name:'222'	,	value:1721	}	,
    	{	name:'223'	,	value:1722	}	,
    	{	name:'224'	,	value:1723	}	,
    	{	name:'225'	,	value:1724	}	,
    	{	name:'226'	,	value:1725	}	,
    	{	name:'227'	,	value:1726	}	,
    	{	name:'228'	,	value:1727	}	,
    	{	name:'229'	,	value:1728	}	,
    	{	name:'230'	,	value:1729	}	,
    	{	name:'231'	,	value:1730	}	,
    	{	name:'232'	,	value:1731	}	,
    	{	name:'233'	,	value:1732	}	,
    	{	name:'234'	,	value:1733	}	,
    	{	name:'235'	,	value:1734	}	,
    	{	name:'236'	,	value:1735	}	,
    	{	name:'237'	,	value:1736	}	,
    	{	name:'238'	,	value:1737	}	,
    	{	name:'239'	,	value:1738	}	,
    	{	name:'240'	,	value:1739	}	,
    	{	name:'241'	,	value:1740	}	,
    	{	name:'242'	,	value:1741	}	,
    	{	name:'243'	,	value:1742	}	,
    	{	name:'244'	,	value:1743	}	,
    	{	name:'245'	,	value:1744	}	,
    	{	name:'246'	,	value:1745	}	,
    	{	name:'247'	,	value:1746	}	,
    	{	name:'248'	,	value:1747	}	,
    	{	name:'249'	,	value:1748	}	,
    	{	name:'250'	,	value:1749	}	,
    	{	name:'251'	,	value:1750	}	,
    	{	name:'252'	,	value:1751	}	,
    	{	name:'253'	,	value:1752	}	,
    	{	name:'254'	,	value:1753	}	,
    	{	name:'255'	,	value:1754	}	,
    	{	name:'256'	,	value:1755	}	,
    	{	name:'257'	,	value:1756	}	,
    	{	name:'258'	,	value:1757	}	,
    	{	name:'259'	,	value:1758	}	,
    	{	name:'260'	,	value:1759	}	,
    	{	name:'261'	,	value:1760	}	,
    	{	name:'262'	,	value:1761	}	,
    	{	name:'263'	,	value:1762	}	,
    	{	name:'264'	,	value:1763	}	,
    	{	name:'265'	,	value:1764	}	,
    	{	name:'266'	,	value:1765	}	,
    	{	name:'267'	,	value:1766	}	,
    	{	name:'268'	,	value:1767	}	,
    	{	name:'269'	,	value:1768	}	,
    	{	name:'270'	,	value:1769	}	

    ]
    var monthScale1 = ['1','2','3','4','5','6','7','8','9'];
    $scope.changeData51 = function(data) {
    	    var positiveP= [];
    	    var positiveT =[];
    	 
//    	    for(var i=0;i<data.length;i++){
//    	    	positiveP.push(data[i].value);
//    	    	var setname =data[i].name/30;
//    	    	var aNew;
//    	    	aNew =setname.toFixed(2);
//    	    	positiveT.push(aNew);
//    	    }
    	     //补全缺损的数据 将没有的数据
            for(var ii=0;ii<270;ii++){
            for(var i=0;i<data.length;i++){
            	  var name = data[i].name;
            	
            	if(name== ii ){
            		positiveP.push(data[i].value);
            		var setname =data[i].name/30;
            		var aNew;
        	    	aNew =setname.toFixed(0);
            		positiveT.push(aNew);
            }else{
            	var nData =ii/30;
            	var toFix = nData.toFixed(0); //重置空月份的日期显示
            	positiveP.push(0);
            	positiveT.push(toFix);
            		}
            	  }
            	}
            
    	    var positivePer=Math.max.apply(Math, positiveP); 
    	    var positivePerMax1 = positivePer/4
    	    var positivePerMax2 = positivePer*2/4;
    	    var positivePerMax3 = positivePer*3/4;
    	    
    	    $scope.option51 = {
    	            title: {

    	            },
    	            tooltip: {
    	                trigger: 'axis'
    	            },
    	            legend: {

    	            },
    	            grid: {
    	                left: '3%',
    	                right: '12%',
    	                bottom: '4%',
    	                containLabel: true
    	            },
    	            toolbox: {
    	                feature: {
    	                    saveAsImage: {}
    	                }
    	            },
    	            xAxis: {
    	            	silent:true,
    	                name : '(月)',
    	                axisLabel:{
    	                    interval:160
    	                },
    	                axisLine:{
    	                    lineStyle:{
    	                        color:'#9D9D9D'
    	                    }
    	                },
//    	                axisLabel:{
//    	                	formatter:'{positiveT}个月'
//    	                },
    	                axisTick:{
    	                    show:false
    	                },
    	                type: 'category',
    	                boundaryGap: false,
    	                data: positiveT ,
    	            },
    	            yAxis : [
    	                {
    	                	
    	                	splitLine: {
    	                        show: false
    	                    },
    	                	 axisTick:{
    	                         show:false
    	                     },
    	                     axisLine:{
    	                         lineStyle:{
    	                             color:'#9D9D9D'
    	                         }
    	                     },
    	                    name : '(人)',
    	                    axisLabel: {
    	                        formatter: '{value}万 '
    	                    },
    	                }
    	            ],

    	            visualMap: {

    	                top: 1110,
    	                right: 1110,
    	                pieces: [{
    	                    gt: 0,
    	                    lte: positivePerMax1,
    	                    color: '#069d6b'
    	                }, {
    	                    gt: positivePerMax1,
    	                    lte: positivePerMax2,
    	                    color: '#ffe455'
    	                }, {
    	                    gt: positivePerMax2,
    	                    lte: positivePerMax3,
    	                    color: '#ffa245'
    	                }],
    	                outOfRange: {
    	                    color: '#cc0033'
    	                }
    	            },
    	            series: [
    	                {
    	                    name:'人',
    	                    type:'line',


    	                    data:positiveP,

    	                    markLine: {
    	                        symbolSize:Array,
    	                        label:{
    	                            emphasis:{
    	                                show:  false ,

    	                            },
    	                        },
    	                        lineStyle:{
    	                            normal :{
    	                                type:'soild',
    	                                width:1,
    	                            },
    	                        },
    	                        silent: true,
    	                        data: [{
    	                            yAxis: positivePerMax1
    	                        }, {
    	                            yAxis: positivePerMax2
    	                        }, {
    	                            yAxis: positivePerMax3
    	                        }]
    	                    },
    	                },
    	            ]
    	        };
	}

    
    var myChart51 = echarts.init(document.getElementById('salesman-top-middle'));
    //myChart51.setOption(option51);

    
    /*
     *     转正后第一次晋升时间数据
     *     
     *     monthScale : 转正后晋升的时间以月为单位
     *     pThird : 转正人员数量
     *     
     * */
        var peoplethird = [
            {name:'15',value:1500},
            {name:'25',value:1600},
            {name:'35',value:2200}
        ];
        $scope.changeData52 = function(data){
        	 var pThird = [];
             var monthScale =[];
           
             for(var iii=0;iii<data.length;iii++){
             	pThird.push(data[iii].value);
             	monthScale.push(data[iii].name/30);
             }
             //补全缺损的数据 将没有的数据
//             for(var ii=0;ii<450;ii++){
//             for(var i=0;i<data.length;i++){
//             	  var name = data[i].name;
//             	 var value = data[i].value;
//             	if(name== ii ){
//             		pThird.push(data[i].value);
//             		var setname =data[i].name/30;
//             		var aNew ;
//             		aNew = setname.toFixed(2);
//             		monthScale.push(aNew);
//             }else{
//            	 pThird.push(0);
//            	 var nData = ii/30;
//            	 var toFix = nData.toFixed(0); //重置空置的日期
//            	 monthScale.push(toFix);
//             		}
//             	  }
//             	}
             
             //15个月名字
             var	Jan	=0;
             var	Feb	=0 ;
             var	Mar=0 ;	
             var	Apr	=0 ;
             var	May	=0 ;
             var	Jun=0 ;	
             var	Jul	=0 ;
             var	Aug=0;	
             var	Sep	=0 ;
             var	Oct=0 ;	
             var	Nov=0;
             var	Dec=0 ;	
             var	Janone=0 ;
             var	Febtwo=0;
             var	Marthr=0 ;	

             
             /*---总计---*/
             for (var i=0; i<data.length;i++){
            	 var name = data[i].name;
            	 var value = data[i].value;
            	  
       try {
            	 		if(name < 30){
            	 			Jan =  Number(data[i].value)+Number(data[i+1].value);
            	 }   else if(name < 60){
            	 			Feb =  Number(data[i].value)+Number(data[i+1].value);
            	 }   else if(name < 90){
            		 Mar =  Number(data[i].value)+Number(data[i+1].value);
            	 }  else if(name < 120){
            		 Apr =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 150){
            		 May =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 180){
            		 Jun =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 210){
            		 Jul =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 240){
            		 Aug =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name <270){
            		 Sep =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 300){
            		 Oct =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 330){
            		 Nov =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 360){
            		 Dec =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 390){
            		 Janone =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 420){
            		 Febtwo =  Number(data[i].value)+Number(data[i+1].value);
            	 } else if(name < 451){
            		 Marthr =  Number(data[i].value)+Number(data[i+1].value);
            	 } 
            	 
       }catch (e){
    	   
       };
   }
             
             
             var peoplethirdMax=Math.max.apply(Math, pThird); 
             var peoplethirdMax1 = peoplethirdMax/4;
             var peoplethirdMax2 =	peoplethirdMax*2/4;
             var peoplethirdMax3 = peoplethirdMax*3/4;
             $scope.option52 = {
            	        title: {

            	        },
            	        tooltip: {
            	            trigger: 'axis',
            	            padding: [5,10],
            	            formatter: "{b} :  {c} ",
            	            extraCssText: 'border-radius: 5px',
            	            axisPointer:{
            	                type: 'shadow',
            	                shadowStyle:{
            	                    opacity:0
            	                }
            	            }
            	        },
            	        legend: {

            	        },
            	        grid: {
            	            left: '3%',
            	            right: '12%',
            	            bottom: '4%',
            	            containLabel: true
            	        },
            	        toolbox: {
            	            feature: {
            	                saveAsImage: {}
            	            }
            	        },
            	        xAxis: {
            	        	
            	            name : '(月)',
            	            axisLine:{
            	                lineStyle:{
            	                    color:'#9D9D9D'
            	                }
            	            },
            	            axisLabel:{
        	                    interval:0  //x轴数据刻度
        	                },
            	            type: 'category',
            	            boundaryGap: false,
            	            data:['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15']
            	        },
            	        yAxis : [
            	            {
            	            	splitLine: {
            	                    show: false
            	                },
            	                axisLine:{
            	                    lineStyle:{
            	                        color:'#9D9D9D'
            	                    }
            	                },
            	                name : '(人)',
            	                axisLabel: {
            	                    formatter: '{value} '
            	                },
            	            }
            	        ],

            	        visualMap: {

            	            top: 1110,
            	            right: 1110,
            	            pieces: [{
            	                gt: 0,
            	                lte: peoplethirdMax1,
            	                color: '#069d6b'
            	            }, {
            	                gt: peoplethirdMax1,
            	                lte: peoplethirdMax2,
            	                color: '#ffe455'
            	            }, {
            	                gt: peoplethirdMax2,
            	                lte: peoplethirdMax3,
            	                color: '#ffa245'
            	            }],
            	            outOfRange: {
            	                color: '#cc0033'
            	            }
            	        },
            	        series: [
            	            {
            	                name:'人',
            	                type:'line',


            	                data:[Jan,	Feb,	Mar,	Apr,	May,	Jun,	Jul,	Aug,	Sep,	Oct,	Nov,	Dec,	Janone,	Febtwo,	Marthr],

            	                markLine: {
            	                    symbolSize:Array,
            	                    label:{
            	                        emphasis:{
            	                            show:  false ,

            	                        },
            	                    },
            	                    lineStyle:{
            	                        normal :{
            	                            type:'soild',
            	                            width:1,
            	                        },
            	                    },
            	                    silent: true,
            	                    data: [{
            	                        yAxis: peoplethirdMax1
            	                    }, {
            	                        yAxis: peoplethirdMax2
            	                    }, {
            	                        yAxis: peoplethirdMax3
            	                    }]
            	                },
            	            },
            	        ]
            	    };
        }

    var myChart52 = echarts.init(document.getElementById('salesman-top-right'));
    //myChart52.setOption(option52);
    
    
    /*
     * 离职人员留存时间
     * retainedPersonnel:元数据
     * retainedP :人员
     * monthRetain:时间
     * */
    //var monthSecond =['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']

    var retainedPersonnel =[ 
    	{	name:'1'	,	value:1500	}	,
    	{	name:'2'	,	value:1501	}	,
    	{	name:'3'	,	value:1502	}	,
    	{	name:'4'	,	value:1503	}	,
    	{	name:'5'	,	value:1504	}	,
    	{	name:'6'	,	value:1505	}	,
    	{	name:'7'	,	value:1506	}	,
    	{	name:'8'	,	value:1507	}
        ]
    $scope.changeData60 = function(data) {
    	    var retainedP = [];
    	    var monthRetain =[];
//    	    for(var i=0;i<data.length;i++){
//    	    	retainedP.push(data[i].value);
//    	    	monthRetain.push(data[i].name/30);
//    	    }
    	    
//    	    //数据补全
//            for(var ii=0;ii<730;ii++){
//                for(var i=0;i<data.length;i++){
//                	  var name = data[i].name;
//                	
//                	if(name== ii ){
//                		retainedP.push(data[i].value);
//                		var setname =data[i].name/30;
//                		var aNew;
//            	    	aNew =setname.toFixed(0);
//            	    	monthRetain.push(aNew);
//                }else{
//                	var nData =ii/30;
//                	var toFix = nData.toFixed(0); //重置空月份的日期显示
//                	retainedP.push(0);
//                	monthRetain.push(toFix);
//                		}
//                	  }
//                	}
            
            //24个月名字
            var	Jan	=0;
            var	Feb	=0;
            var	Mar=0	;
            var	Apr=0	;
            var	May=0	;
            var	Jun=0	;
            var	Jul=0	;
            var	Aug=0	;
            var	Sep=0	;
            var	Oct	=0;
            var	Nov=0	;
            var	Dec=0	;
            var	Janone=0	;
            var	Febone	=0;
            var	Marone	=0;
            var	Aprone=0	;
            var	Mayone=0	;
            var	Junone	=0;
            var	Julone	=0;
            var	Augone	=0;
            var	Sepone	=0;
            var	Octone=0	;
            var	Novone=0	;
            var	Decone	=0;


            
            /*---总计---*/
            for (var i=0; i<data.length;i++){
           	 var name = data[i].name;
           	 var value = data[i].value;
           	  
      try {
           	 		if(name < 30){
           	 			Jan =  Number(data[i].value)+Number(data[i+1].value);
           	 }   else if(name < 60){
           	 			Feb =  Number(data[i].value)+Number(data[i+1].value);
           	 }   else if(name < 90){
           		 Mar =  Number(data[i].value)+Number(data[i+1].value);
           	 }  else if(name < 120){
           		 Apr =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 150){
           		 May =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 180){
           		 Jun =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 210){
           		 Jul =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 240){
           		 Aug =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name <270){
           		 Sep =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 300){
           		 Oct =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 330){
           		 Nov =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 360){
           		 Dec =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 390){
           		 Janone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 420){
           		 Febone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 450){
           		 Marone =  Number(data[i].value)+Number(data[i+1].value);
           	 }  else if(name <480){
           		 Aprone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 510){
           		 Mayone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 540){
           		 Junone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 570){
           		Julone=  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 600){
           		 Augone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 630){
           		 Sepone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 660){
           		 Octone=  Number(data[i].value)+Number(data[i+1].value);
           	 }  else if(name < 690){
           		Novone =  Number(data[i].value)+Number(data[i+1].value);
           	 } else if(name < 720){
           		Decone =  Number(data[i].value)+Number(data[i+1].value);
           	 } 
           	 
      }catch (e){
   	   
      };
  }
    	    $scope.option60 = {
    	            title: {

    	            },
    	            tooltip: {
    	                trigger: 'axis',
    	                padding: [5,10],
    	                formatter: "{b} :  {c} ",
    	                extraCssText: 'border-radius: 5px',
    	                axisPointer:{
    	                    type: 'shadow',
    	                    shadowStyle:{
    	                        opacity:0
    	                    }
    	                }
    	            },
    	            toolbox: {
    	                feature: {
    	                    saveAsImage: {}
    	                }
    	            },
    	            grid: {
    	                left: '3%',
    	                right: '14%',
    	                bottom: '3%',
    	                containLabel: true
    	            },
    	            xAxis : [
    	                {
    	                    name: '(月)',
    	                    type : 'category',
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    },
    	                    axisLabel:{			// echarts 中X轴上显示的数据
//        	                   formatter: function (  retainedP, monthRetain){
//        	                	   // 格式化成月/日，只在第一个刻度显示年份
//        	                	    var date = new Date(monthRetain);
//        	                	    var texts = (date.getMonth() + 1);
//        	                	    return texts.join('/');
//        	                   }
    	                    	interval:1,
        	                },
    	                    boundaryGap : false,
    	                    data : [ '1',	 '2',	 '3',	 '4',	 '5',	 '6',	 '7',	 '8',	 '9',	 '10',	 '11',	 '12',	 '13',	 '14',	 '15',	 '16',	 '17',	 '18',	 '19',	 '20',	 '21',	 '22',	 '23',	 '24']
    	                }
    	            ],
    	            yAxis : [
    	                {
    	                	 axisLine:{
    	                         lineStyle:{
    	                             color:'#9D9D9D'
    	                         }
    	                     },
    	                    name: '(人)',
    	                    type : 'value'
    	                }
    	            ],
    	            series : [

    	                {
    	                    name:'人',
    	                    type:'line',
    	                    stack: '总量',
    	                    itemStyle:{
    	                        normal:{
    	                            color:'#1fa9eb',  //曲线颜色
    	                        }
    	                    },
    	                    areaStyle:{
    	                        normal:{
    	                            color: {
    	                                type: 'linear',//线性渐变
    	                                x: 0,
    	                                y: 0.5,
    	                                x2: 0,
    	                                y2: 1,
    	                                colorStops: [{
    	                                    offset: 0, color: '#e0ecfb' // 0% 处的颜色
    	                                }, {
    	                                    offset: 1, color: '#ffffff' // 100% 处的颜色
    	                                }],
    	                                globalCoord: false // 缺省为 false
    	                            }
    	                        }
    	                    },
    	                    label: {
    	                        normal: {

    	                            show: false,
    	                            position: 'top'

    	                        },
    	                        lineStyle:{
    	                            normal:{
    	                                color:'blue',
    	                            }
    	                        },



    	                    },


    	                    data:[Jan,	Feb,	Mar,	Apr,	May,	Jun,	Jul,	Aug,	Sep,	Oct,	Nov,	Dec,
    	                    	Janone,	Febone,	Marone,	Aprone,	Mayone,	Junone,	Julone,	Augone,	Sepone,	Octone,	Novone,	Decone
],
    	                }
    	            ]
    	        };
	}

    var myChart60 = echarts.init(document.getElementById('salesman-bottom-left'));
    //myChart60.setOption(option60);

/*
 * 入职后前3/6个月平均Fyc
 *  fycData:元数据
 *  fycP: 入职人员
 *  monthM:金额
 * */
    var fycData =[
        {name:'1',value:1500},
        {name:'2',value:1600},
        {name:'3',value:2200},
        {name:'4',value:2500},
        {name:'5',value:1500},
        {name:'6',value:1600},
        {name:'7',value:2200},
        {name:'8',value:2500},
        {name:'9',value:1500},
        {name:'10',value:1600},
        {name:'11',value:2200},
        {name:'12',value:2500},
        {name:'12',value:1500},
        {name:'13',value:1600},
        {name:'14',value:2200},
        {name:'15',value:2500},
        {name:'16',value:1500},
        {name:'17',value:1600},
        {name:'18',value:2200},
        {name:'19',value:2500},
        {name:'20',value:1500},
        {name:'21',value:1600},
        {name:'22',value:2200},
        {name:'23',value:2500},
        {name:'24',value:1500},
        {name:'25',value:1600},
        {name:'26',value:2200},
        {name:'27',value:2500},
        {name:'28',value:1500},
        {name:'29',value:1600},
        {name:'30',value:2200},
        {name:'31',value:2500}
    ]
//       var fycScale = ['0','0.1','0.2','0.3','0.4','0.5','0.6','0.7','0.8','0.9','1',
//        '1.1','1.2','1.3','1.4','1.5','1.6','1.7','1.8','1.9','2',
//        '2.1','2.2','2.3','2.4','2.5','2.6','2.7','2.8','2.9','3',
//        '3','3.1','3.2','3.3','3.4','3.5','3.6','3.7','3.8','3.9','4',
//        '4','4.1','4.2','4.3','4.4','4.5','4.6','4.7','4.8','4.9','5'
//    ]
    $scope.changeData70 = function(data){
//    	 var fycP = [];
//    	    var monthM =[];
//    	    for(var i=0;i<data.length;i++){
//    	    	fycP.push(data[i].value);
//    	    	monthM.push(data[i].name);
//    	    }
    	    //修改数据格式

    	        var fycP = [];
    	        var monthM =[];
    	     //补全缺损的数据 将没有的数据
    	        for(var ii=0;ii<51;ii++){
    	        for(var i=0;i<data.length;i++){
    	        	  var name = data[i].name;
    	        	if(name== ii ){
    	        		var newdate = data[i].name/10;
    	        		fycP.push(data[i].value);
    	        		monthM.push(newdate);
    	        }else if(name>50 ){
    	        	fycP.push(data[i].value);
    	        	monthM.push("以上");
    	        }
    	        	else{
    	        		fycP.push(0);
    	        	monthM.push(ii/10); 
    	        		}
    	        	  }
    	        	}
    	        
            
    	    $scope.option70 = {
    	            title: {

    	            },
    	            tooltip: {
    	                trigger: 'axis',
    	                padding: [5,10],
    	                formatter: "{b} :  {c} ",
    	                extraCssText: 'border-radius: 5px',
    	                axisPointer:{
    	                    type: 'shadow',
    	                    shadowStyle:{
    	                        opacity:0
    	                    }
    	                }
    	            },
    	            toolbox: {
    	                feature: {
    	                    saveAsImage: {}
    	                }
    	            },
    	            grid: {
    	                left: '3%',
    	                right: '14%',
    	                bottom: '3%',
    	                containLabel: true
    	            },
    	            xAxis : [
    	                {
    	                    name: '(元)',
    	                    axisLabel: {
    	                        formatter: '{value} 万'
    	                       
    	                    },
    	                    axisLabel: {
    	                    	 interval:24  //x轴数据刻度
    	                    },
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    },
    	                    boundaryGap : false,
    	                    data : monthM,
    	                }
    	            ],
    	            yAxis : [
    	                {
    	                    name: '(人)',
    	                    axisLabel: {
    	                        formatter: '{value} '
    	                    },
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    },
    	                }
    	            ],
    	            series : [
    	                {
    	                    name:'人',
    	                    type:'line',
    	                    stack: '总量',
    	                    itemStyle:{
    	                        normal:{
    	                            color:'#1fa9eb',  //曲线颜色
    	                        }
    	                    },
    	                    areaStyle:{
    	                        normal:{
    	                            color: {
    	                                type: 'linear',//线性渐变
    	                                x: 0,
    	                                y: 0.5,
    	                                x2: 0,
    	                                y2: 1,
    	                                colorStops: [{
    	                                    offset: 0, color: '#e0ecfb' // 0% 处的颜色
    	                                }, {
    	                                    offset: 1, color: '#ffffff' // 100% 处的颜色
    	                                }],
    	                                globalCoord: false // 缺省为 false
    	                            }
    	                        }
    	                    },
    	                    label: {
    	                        normal: {

    	                            show: false,
    	                            position: 'top'

    	                        },
    	                        lineStyle:{
    	                            normal:{
    	                                color:'blue',
    	                            }
    	                        },
    	                    },
    	                    data:fycP,
    	                }
    	            ]
    	        };
    }
    var myChart70 = echarts.init(document.getElementById('salesman-bottom-right'));
    //myChart70.setOption(option70);
    
    $(window).resize(function() {
		setTimeout(function() {
			myChart50.resize();
			myChart51.resize();
			myChart52.resize();
			myChart60.resize();
			myChart70.resize();
		}, 100);
	});
    
    /**
	 * 获取数据
	 */
    $scope.startData = function(){
        $http({
			method:'post',
			url:context+'/empnoAnalysis/getAllAnalysis',
			data:angular.toJson($scope.reqData)
		}).success(function(data){
			$rootScope.excelData = data;
			$scope.changeData50(angular.fromJson(data.firstDays));
			myChart50.setOption($scope.option50);
			$scope.changeData51(angular.fromJson(data.regularDays));
			myChart51.setOption($scope.option51);
			$scope.changeData52(angular.fromJson(data.promotionDays));
			myChart52.setOption($scope.option52);
			$scope.changeData60(angular.fromJson(data.stayDays));
			myChart60.setOption($scope.option60);
			$scope.changeData70(angular.fromJson(data.threeMonthFyc));
			myChart70.setOption($scope.option70);
			console.log(data.threeMonthFyc)
		});

    };
    $scope.startData();
    
    //营销员FYC下拉框操作
    $scope.month = '3';
    $scope.service = 'outJob';
    $scope.fycChange = function() {
    	$scope.reqData.month= $scope.month;
    	$scope.reqData.service=$scope.service; 
    	 $http({
 			method:'post',
 			url:context+'/empnoAnalysis/fyc',
 			data:angular.toJson($scope.reqData)
 		}).success(function(data){
 			console.log('data', data);
 			$scope.changeData70(angular.fromJson(data.fyc));
			myChart70.setOption($scope.option70);
 		});

    };

});