//分类大数据分析_客群件均分析
app.controller("averageCtrl", function ($scope,$http,$rootScope) {
	$scope.context=context;
	//筛选条件
	$scope.reqData={};
	$scope.reqData.personMark = '阳光青年';
	$scope.reqData.beginDate=$rootScope.startDate;
	$scope.reqData.endDate=$rootScope.endDate;
	$scope.reqData.branch=$rootScope.branchCode;
	
	//监听分公司下拉选
	$scope.$watch('branchCode', function() {
		console.log('branchCode', $scope.branchCode);
		if($scope.branchCode==null){
			$scope.reqData.branch='';
		}else{
			$scope.reqData.branch=$scope.branchCode;
		}
		$scope.startData();
	});
	
	//时间控件起始时间
	$("#start-date").on("change",function(){
		$scope.reqData.beginDate=$rootScope.startDate;
		$scope.startData();
	});
	//时间控件结束时间
	$("#end-date").on("change",function(){
		$scope.reqData.endDate=$rootScope.endDate;
		$scope.startData();
	});

    //四大客群客均件数
    var averageDeals = [
        {name:'阳光青年',value:1500},
        {name:'青年才俊',value:1600},
        {name:'家大业大',value:2200},
        {name:'小康之家',value:2500}
    ];
    $scope.changeData30 = function(data){
    	    var client1 = [];
    	    var num1 = [];
    	    for(var i=0;i<data.length;i++){
    	        client1.push(data[i].name);
    	        num1.push(data[i].value);
    	    }
    	    $scope.option30 = {
    	    		 tooltip: {
    	    	            trigger: 'axis',
    	    	            padding: [5,10],
    	    	            formatter: "{b} :  {c} ",
    	    	            extraCssText: 'border-radius: 5px',
    	    	            axisPointer:{
    	    	                type: 'shadow',
    	    	                shadowStyle:{
    	    	                    opacity:0
    	    	                }
    	    	            }
    	    	        },
    	            grid:{
    	                top:30,
    	                bottom:40,
    	                left: '15%',
    	                right: '15%'
    	            },
    	            toolbox: {
    	                show : true,
    	                feature : {
    	                    saveAsImage : {show: true}
    	                },
    	                bottom: 10,
    	                right: 0
    	            },
    	            xAxis: [
    	                {
    	                    type: 'category',
    	                    data: client1,
    	                    axisPointer: {
    	                        type: 'shadow'
    	                    },
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    }
    	                }
    	            ],
    	            yAxis: [
    	                {
    	                    type: 'value',
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    },
    	                    label: {
    	                        normal: {
    	                            show: true,
    	                            position: 'top'
    	                        }
    	                    }
    	                }
    	            ],
    	            series: [
    	                {
    	                    type:'bar',
    	                    barWidth: '40%',
    	                    itemStyle:{
    	                        normal:{
    	                            color:'#86bbe2'
    	                        },
    	                        emphasis:{
    	                            color:'#2493ca'
    	                        }
    	                    },
    	                    label: {
    	                        normal: {
    	                            show: true,
    	                            position: 'top',
    	                            textStyle:{
    	                            	color: '#737373'
    	                            }
    	                        }
    	                    },
    	                    data:num1
    	                }
    	            ]
    	        };
    }
    var myChart30 = echarts.init(document.getElementById('average-top-right'));
    //myChart30.setOption(option30);

    //四大客群客均保费
    var averageFees = [
        {name:'阳光青年',value:1500},
        {name:'青年才俊',value:1600},
        {name:'家大业大',value:1200},
        {name:'小康之家',value:2500}
    ];
    $scope.changeData31 = function(data){
    	var client2 = [];
        var num2 = [];
        for(var i=0;i<data.length;i++){
            client2.push(data[i].name);
            num2.push(data[i].value);
        }
        $scope.option31 = {
        		 tooltip: {
        	            trigger: 'axis',
        	            padding: [5,10],
        	            formatter: "{b} :  {c} ",
        	            extraCssText: 'border-radius: 5px',
        	            axisPointer:{
        	                type: 'shadow',
        	                shadowStyle:{
        	                    opacity:0
        	                }
        	            }
        	        },
                grid:{
                    top:30,
                    bottom:40,
                    left: '15%',
                    right: '15%'
                },
                toolbox: {
                    show : true,
                    feature : {
                        saveAsImage : {show: true}
                    },
                    bottom: 10,
                    right: 0
                },
                xAxis: [
                    {
                        type: 'category',
                        data: client2,
                        axisPointer: {
                            type: 'shadow'
                        },
                        axisLine:{
                            lineStyle:{
                                color:'#9D9D9D'
                            }
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value} 千元'
                        },
                        axisLine:{
                            lineStyle:{
                                color:'#9D9D9D'
                            }
                        },
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        }
                    }
                ],
                series: [
                    {
                        type:'bar',
                        barWidth: '40%',
                        itemStyle:{
                            normal:{
                                color:'#f5e36b'
                            },
                            emphasis:{
                                color:'#e8d23c'
                            }
                        },
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
	                            textStyle:{
	                            	color: '#737373'
	                            }
                            }
                        },
                        data:num2
                    }
                ]
            };
    }
    
    var myChart31 = echarts.init(document.getElementById('average-bottom-left'));
    //myChart31.setOption(option31);

    //四大客群件均保额
    //默认选择阳光青年数据显示
    $scope.clientGroup = 1;
    var averCoverages = [
        {name:'意外',value:2500},
        {name:'重疾健康',value:1600},
        {name:'分红',value:1200},
        {name:'寿险',value:1800}
    ];
    //数据转换格式
    $scope.changeData32 = function(data){
    	 var insurances = [];
    	    var coverage = [];
    	    for(var i=0;i<data.length;i++){
    	        insurances.push(data[i].name);
    	        coverage.push(data[i].value);
    	    }
    	    $scope.option32 =  {
    	    		 tooltip: {
    	    	            trigger: 'axis',
    	    	            padding: [5,10],
    	    	            formatter: "{b} :  {c} ",
    	    	            extraCssText: 'border-radius: 5px',
    	    	            axisPointer:{
    	    	                type: 'shadow',
    	    	                shadowStyle:{
    	    	                    opacity:0
    	    	                }
    	    	            }
    	    	        },
    	            grid:{
    	                top:30,
    	                bottom:40,
    	                left: '15%',
    	                right: '15%'
    	            },
    	            toolbox: {
    	                show : true,
    	                feature : {
    	                    saveAsImage : {show: true}
    	                },
    	                bottom: 10,
    	                right: 0
    	            },
    	            xAxis: [
    	                {
    	                    type: 'category',
    	                    data: insurances,
    	                    axisPointer: {
    	                        type: 'shadow'
    	                    },
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    }
    	                }
    	            ],
    	            yAxis: [
    	                {
    	                    type: 'value',
    	                    axisLabel: {
    	                        formatter: '{value} 千元'
    	                    },
    	                    axisLine:{
    	                        lineStyle:{
    	                            color:'#9D9D9D'
    	                        }
    	                    },
    	                    label: {
    	                        normal: {
    	                            show: true,
    	                            position: 'top'
    	                        }
    	                    }
    	                }
    	            ],
    	            series: [
    	                {
    	                    type:'bar',
    	                    barWidth: '40%',
    	                    itemStyle:{
    	                        normal:{
    	                            color:'#aee387'
    	                        },
    	                        emphasis:{
    	                            color:'#609339'
    	                        }
    	                    },
    	                    label: {
    	                        normal: {
    	                            show: true,
    	                            position: 'top',
    	                            textStyle:{
    	                            	color: '#737373'
    	                            }
    	                        }
    	                    },
    	                    data: coverage
    	                }
    	            ]
    	        };
    }
   
    var myChart32 = echarts.init(document.getElementById('average-bottom-right'));
    //myChart32.setOption(option32);

    $(window).resize(function(){
        setTimeout(function(){
            myChart30.resize();
            myChart31.resize();
            myChart32.resize();
        },100);
    });
    
    /**
	 * 获取数据
	 */
    $scope.startData = function(){
        $http({
			method:'post',
			url:context+'/customerTypeAverageAnalysis/allCustomerTypeAverageAnalysis',
			data:angular.toJson($scope.reqData)
		}).success(function(data){
			console.log('data', data);
			$rootScope.excelData = data;
			$scope.average_policyno =  data.average_policyno;
			$scope.average_money =  data.average_money;
			$scope.changeData30(angular.fromJson(data.fourTypeAveragePolicyno));
			myChart30.setOption($scope.option30);
			$scope.changeData31(angular.fromJson(data.fourTypeAverageMoney));
			myChart31.setOption($scope.option31);
			$scope.changeData32(angular.fromJson(data.fourTypeAverageMoneyForClassType));
			myChart32.setOption($scope.option32);
		});

    };
    $scope.startData();
    
  //四大客群下拉框操作
    $scope.clientGroup='阳光青年';
    $scope.choseGroup = function() {
    	$scope.reqData.personMark = $scope.clientGroup;
    	$scope.startData();
    };

});