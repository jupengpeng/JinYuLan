//分类大数据分析_客群分析
app.controller("customersCtrl", function ($scope,$http,$rootScope) {
	$scope.context=context;
	$scope.reqData={};
	$scope.reqData.riskNum = 1;
	$scope.reqData.beginDate=$rootScope.startDate;
	$scope.reqData.endDate=$rootScope.endDate;
    $scope.reqData.branch=$rootScope.branchCode;
	
	//监听分公司下拉选
	$scope.$watch('branchCode', function() {
		console.log('branchCode', $scope.branchCode);
		if($scope.branchCode==null){
			$scope.reqData.branch='';
		}else{
			$scope.reqData.branch=$scope.branchCode;
		}
		$scope.startData();
	});
	
	//时间控件起始时间
	$("#start-date").on("change",function(){
		$scope.reqData.beginDate=$rootScope.startDate;
		$scope.startData();
	});
	//时间控件结束时间
	$("#end-date").on("change",function(){
		$scope.reqData.endDate=$rootScope.endDate;
		$scope.startData();
	});
    //阳光青年
    /*var client0 = [
        {name:'意外险',value:1500,fee:300},
        {name:'寿险',value:600,fee:100},
        {name:'车险',value:2200,fee:200},
        {name:'健康险',value:1200,fee:150},
        {name:'养老险',value:900,fee:300}
    ];*/
    var myChart10 = echarts.init(document.getElementById('customers-top-left'));
    var option10 = {
        backgroundColor:'#fff',
        tooltip: {
        	show: false,
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            },
            bottom: 10,
            right: 0
        },
        calculable : true,
        series: [
            {
            	name: '保单件数',
                type:'pie',
                radius: ['15%', '70%'],
                roseType : 'radius',
                label: {
                    normal: {
                        show: true,
                        textStyle : {
							color: '#737373'
						},
						align: 'left',
                        formatter:function(params){
                            return params.name+'\n'+'保单件数'+params.value+" ("+params.percent+"%)"
                            +'\n'+'件均保费：'+params.data.fee;
                        }
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                        	color: '#737373'
//                            fontSize: '30',
//                            fontWeight: 'bold'
                        },
                        position:'center'
                    }
                },
                labelLine: {
                	normal:{
                		lineStyle:{
                			color: '#D0D0D0'
                		}
                	}
                },
                itemStyle:{
                    normal:{
                        color: function(params) {
                            var colorList = [
                                '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                            ];
                            return colorList[params.dataIndex]
                        }
                    }
                },
                data: []
            }
        ]
    };
    //myChart10.setOption(option10);

    //青年才俊
    var myChart11 = echarts.init(document.getElementById('customers-top-right'));
    var option11 = {
        backgroundColor:'#fff',
        tooltip: {
        	show: false,
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            },
            bottom: 10,
            right: 0
        },
        calculable : true,
        series: [
            {
            	name: '保单件数',
                type:'pie',
                radius: ['15%', '70%'],
                roseType : 'radius',
                label: {
                    normal: {
                        show: true,
                        color: '#737373',
                        position: 'outside',
                        formatter:function(params){
                            return params.name+'：保单件数'+params.value+" ("+params.percent+"%)"
                            +'\n'+'件均保费：'+params.data.fee;
                        }
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
//                            fontSize: '30',
//                            fontWeight: 'bold'
                        },
                        position:'center'
                    }
                },
                labelLine: {
                	normal:{
                		lineStyle:{
                			color: '#D0D0D0'
                		}
                	}
                },
                itemStyle:{
                    normal:{
                        color: function(params) {
                            var colorList = [
                                '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                            ];
                            return colorList[params.dataIndex]
                        }
                    }
                },
                data: []
            }
        ]
    };
    //myChart11.setOption(option11);

    //小康之家
    var myChart12 = echarts.init(document.getElementById('customers-bottom-left'));
    var option12 = {
        backgroundColor:'#fff',
        tooltip: {
        	show: false,
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            },
            bottom: 10,
            right: 0
        },
        calculable : true,
        series: [
            {
            	name: '保单件数',
                type:'pie',
                radius: ['15%', '70%'],
                roseType : 'radius',
                label: {
                    normal: {
                        show: true,
                        color: '#737373',
                        position: 'outside',
                        formatter:function(params){
                            return params.name+'：保单件数'+params.value+" ("+params.percent+"%)"
                            +'\n'+'件均保费：'+params.data.fee;
                        }
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
//                            fontSize: '30',
//                            fontWeight: 'bold'
                        },
                        position:'center'
                    }
                },
                labelLine: {
                	normal:{
                		lineStyle:{
                			color: '#D0D0D0'
                		}
                	}
                },
                itemStyle:{
                    normal:{
                        color: function(params) {
                            var colorList = [
                                '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                            ];
                            return colorList[params.dataIndex]
                        }
                    }
                },
                data: []
            }
        ]
    };
    //myChart12.setOption(option12);

    //家大业大
    var myChart13 = echarts.init(document.getElementById('customers-bottom-right'));
    var option13 = {
        backgroundColor:'#fff',
        tooltip: {
        	show: false,
            trigger: 'item',
            formatter: "{a} {b}: {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            },
            bottom: 10,
            right: 0
        },
        calculable : true,
        series: [
            {
            	name: '保单件数',
                type:'pie',
                radius: ['15%', '70%'],
                roseType : 'radius',
                label: {
                    normal: {
                        show: true,
                        color: '#737373',
                        position: 'outside',
                        formatter:function(params){
                            return params.name+'：保单件数'+params.value+" ("+params.percent+"%)"
                            +'\n'+'件均保费：'+params.data.fee;
                        }
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
//                            fontSize: '30',
//                            fontWeight: 'bold'
                        },
                        position:'center'
                    }
                },
                labelLine: {
                	normal:{
                		lineStyle:{
                			color: '#D0D0D0'
                		}
                	}
                },
                itemStyle:{
                    normal:{
                        color: function(params) {
                            var colorList = [
                                '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                            ];
                            return colorList[params.dataIndex]
                        }
                    }
                },
                data: []
            }
        ]
    };
    //myChart13.setOption(option13);

    $(window).resize(function(){
        setTimeout(function(){
            myChart10.resize();
            myChart11.resize();
            myChart12.resize();
            myChart13.resize();
        },100);
    });
    
    /**
     * 获取数据
     */
    $scope.startData=function(){
    	//查询用户报表
        $http.post(context+"/customerGAController/getData"
        		,angular.toJson($scope.reqData)).success(function (response) {
             console.log('客群分析',response);
             $rootScope.excelData = response;
             option10.series[0].data=response.ygqndata;
             myChart10.setOption(option10);
             
             option11.series[0].data=response.jycjdata;
             myChart11.setOption(option11);
             
             option12.series[0].data=response.xkzjdata;
             myChart12.setOption(option12);
             
             option13.series[0].data=response.jdyddata;
             myChart13.setOption(option13);
         });
    }
    $scope.startData();
});
