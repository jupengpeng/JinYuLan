//分类大数据分析_缴费年限分析
app.controller("paymentCtrl", function ($scope,$http,$rootScope) {
	$scope.context=context;
	
	//筛选条件
	$scope.reqData={};
	$scope.reqData.personMark = '阳光青年';
	$scope.reqData.beginDate=$rootScope.startDate;
	$scope.reqData.endDate=$rootScope.endDate;
	$scope.reqData.branch=$rootScope.branchCode;
	$scope.reqData.classname='';
	
	//监听分公司下拉选
	$scope.$watch('branchCode', function() {
		console.log('branchCode', $scope.branchCode);
		if($scope.branchCode==null){
			$scope.reqData.branch='';
		}else{
			$scope.reqData.branch=$scope.branchCode;
		}
		$scope.startData();
	});
	
	//时间控件起始时间
	$("#start-date").on("change",function(){
		$scope.reqData.beginDate=$rootScope.startDate;
		$scope.startData();
	});
	//时间控件结束时间
	$("#end-date").on("change",function(){
		$scope.reqData.endDate=$rootScope.endDate;
		$scope.startData();
	});

    //保单缴费年限分布
    var totalPay = [
        {name:'三年',value:1500},
        {name:'五年',value:600},
        {name:'趸交',value:2200},
        {name:'其他',value:500},
        {name:'10年15年及以上',value:900}
    ];

    	var myChart20 = echarts.init(document.getElementById('payment-top-left'));
        var option20 = {
            backgroundColor:'#fff',
            tooltip: {
            	show:false,
                trigger: 'item',
                formatter: "{a} <br/>{b}: {c} ({d}%)"
            },
            toolbox: {
                show : true,
                feature : {
                    saveAsImage : {show: true}
                },
                bottom: 10,
                right: 0
            },
            calculable : true,
            series: [
                {
                    type:'pie',
                    radius: ['15%', '70%'],
                    roseType : 'radius',
                    label: {
                        normal: {
                            show: true,
                            textStyle:{
                            	color: '#737373'
                            },
                            position: 'outside',
                            formatter: "{b|{b}}: \n{c} \n({d}%)",
                            rich:{
								b:{
									color: '#324e66',
									fontSize: 13,
			                        lineHeight: 20,
								}
							}
                        }
                    },
                    labelLine:{
                    	normal:{
                    		lineStyle:{
                    			color: '#D0D0D0'
                    		}
                    	}
                    },
                    itemStyle:{
                        normal:{
                            color: function(params) {
                                var colorList = [
                                    '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                                ];
                                return colorList[params.dataIndex]
                            }
                        }
                    },
                    data: []
                }
            ]
        };
        //myChart20.setOption(option20);

    

    // var data = [
    //     {name:'险种十',top1:'趸交',val:'60',other:'60'},
    //     {name:'险种九',top1:'三年',val:'100',other:'80'},
    //     {name:'险种八',top1:'五年',val:'120',other:'100'},
    //     {name:'险种七',top1:'10年15年及以上',val:'150',other:'160'},
    //     {name:'险种六',top1:'趸交',val:'200',other:'190'},
    //     {name:'险种五',top1:'三年',val:'230',other:'230'},
    //     {name:'险种四',top1:'三年',val:'270',other:'260'},
    //     {name:'险种三',top1:'趸交',val:'310',other:'300'},
    //     {name:'险种二',top1:'五年',val:'350',other:'330'},
    //     {name:'险种一',top1:'三年',val:'400',other:'400'}
    // ];

    //前十主力险种缴费年限
    /*var data1 =[
        {name:'险种十',value:'趸交'},
        {name:'险种九',value:'三年'},
        {name:'险种八',value:'五年'},
        {name:'险种七',value:'10年15年及以上'},
        {name:'险种六',value:'趸交'},
        {name:'险种五',value:'三年'},
        {name:'险种四',value:'三年'},
        {name:'险种三',value:'趸交'},
        {name:'险种二',value:'五年'},
        {name:'险种一',value:'三年'}
    ];
    var data2 =[
        {name:'险种十',value:'60'},
        {name:'险种九',value:'100'},
        {name:'险种八',value:'120'},
        {name:'险种七',value:'150'},
        {name:'险种六',value:'200'},
        {name:'险种五',value:'230'},
        {name:'险种四',value:'270'},
        {name:'险种三',value:'310'},
        {name:'险种二',value:'350'},
        {name:'险种一',value:'400'}
    ];
    var data3 =[
        {name:'险种十',value:'60'},
        {name:'险种九',value:'80'},
        {name:'险种八',value:'100'},
        {name:'险种七',value:'160'},
        {name:'险种六',value:'190'},
        {name:'险种五',value:'230'},
        {name:'险种四',value:'260'},
        {name:'险种三',value:'300'},
        {name:'险种二',value:'330'},
        {name:'险种一',value:'400'}
    ];

    var len=data1.length;

    //险种
    var types = [];
    for(var i=0;i<len;i++){
        types.push(data1[i].name);1
    }
//    console.log(types);

    //缴费类型
    var pay = [];
    for(var i=0;i<len;i++){
        pay.push(data1[i].value);
    }
//    console.log(pay);

    //缴费类型数量
    var num1 = [];
    var num2 = [];
    for(var i=0;i<len;i++){
        num1.push(data2[i].value);
        num2.push(data3[i].value);
    }
//    console.log(num1);
//    console.log(num2);

    //百分比
    var per1 = [];
    var per2 = []
    for(var i=0;i<len;i++){
        var percent1 = (((parseInt(data2[i].value)/(parseInt(data2[i].value)+parseInt(data3[i].value))))*100).toFixed(1);
        var percent2 = (100 - percent1).toFixed(1);
        per1.push(percent1+'%');
        per2.push(percent2+'%');
    }*/
//    console.log(per1);
//    console.log(per2);
    //修改myChart21所需要的data数据格式
    $scope.changeData21 = function(data){
    	var len=data.length;
        //险种
        var types = [];
        for(var i=len-1;i>=0;i--){
            types.push(data[i].CLASSNAME);
        }
        //缴费类型
        var pay = [];
        for(var i=len-1;i>=0;i--){
            pay.push(data[i].YEARNUM);
        }
        //缴费类型数量
        var num1 = [];
        var num2 = [];
        for(var i=len-1;i>=0;i--){
            num1.push(data[i].COUNT_YEAR);
            num2.push(data[i].COUNT_POLICYNO-data[i].COUNT_YEAR);
        }
      //百分比数据
        var num3 = [];
        var num4 = [];
        for(var i=len-1;i>=0;i--){
            num3.push((((data[i].COUNT_POLICYNO-data[i].COUNT_YEAR)/data[i].COUNT_POLICYNO)*100).toFixed(1)+"%");
            num4.push((((data[i].COUNT_YEAR/data[i].COUNT_POLICYNO))*100).toFixed(1)+"%");
        }
        $scope.option21 = {
        		tooltip: {
        			show: true,
    	            trigger: 'axis',
    	            formatter: function(params){
    	            	console.log(params);
                        return num3[params[0].dataIndex] + '<br/>' + 
                        	   pay[params[0].dataIndex] + '：' + num4[params[0].dataIndex]
                    },
    	            padding: [5,10],
    	            extraCssText: 'border-radius: 5px',
    	            axisPointer:{
    	                type: 'shadow',
    	                shadowStyle:{
    	                    opacity:0
    	                }
    	            }
        	    },
                legend: {
                    data: ['其他缴费类型', '第一缴费类型'],
                    right: 10
                },
                grid: {
                    top: 30,
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis:  {
                    type: 'value',
                    splitLine:{
                        show:false
                    },
                    axisLine:{
                        lineStyle:{
                            color:'#9D9D9D'
                        }
                    }
                },
                yAxis: {
                    type: 'category',
                    data: types,
                    splitLine:{
                        show:false
                    },
                    axisLine:{
                        lineStyle:{
                            color:'#9D9D9D'
                        }
                    }
                },
                series: [
                    {
                        name: '其他缴费类型',
                        type: 'bar',
                        barWidth:'70%',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight',
                                formatter: function(params){
	                            	if(num3[params.dataIndex]!='0.0%'){
	                                	return num3[params.dataIndex]
	                                }else{
	                                    return '';
	                                }
                                }
                            }
                        },
                        itemStyle:{
                            normal:{
                                color:'#3486bb'
                            },
                            emphasis:{
                                color:'#0f79ad'
                            }
                        },
                        data: num2
                    },
                    {
                        name: '第一缴费类型',
                        type: 'bar',
                        barWidth:'60%',
                        stack: '总量',
                        label: {
                            normal: {
                                show: true,
                                textStyle:{
                                	color: '#324e66'
                                },
                                position: 'insideRight',
                                formatter: function(params){
	                            	if(num4[params.dataIndex]!='0%'){
	                                	return pay[params.dataIndex] + '：' + num4[params.dataIndex]
	                                }else{
	                                    return null;
	                                }
                                }
                            }
                        },
                        itemStyle:{
                            normal:{
                                color:'#e9c647'
                            },
                            emphasis:{
                                color:'#f1ca12'
                            }
                        },
                        data: num1
                    }
                ]
            };
    }
    var myChart21 = echarts.init(document.getElementById('payment-top-right'));
    //myChart21.setOption(option21);


    //默认选择阳光青年数据显示
    $scope.clientGroup = 1;
    //四大客群的主要缴费年限
    var clientPay = [
        {name:'三年',value:1500},
        {name:'五年',value:600},
        {name:'趸交',value:2200},
        {name:'其他',value:500},
        {name:'10年15年及以上',value:900}
    ];
    var myChart22 = echarts.init(document.getElementById('payment-bottom-left'));
    var option22 = {
        backgroundColor:'#fff',
        tooltip: {
        	show:false,
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            },
            bottom: 10,
            right: 0
        },
        calculable : true,
        series: [
            {
                type:'pie',
                radius: ['15%', '70%'],
                roseType : 'radius',
                label: {
                    normal: {
                        show: true,
                        textStyle:{
                        	color: '#737373'
                        },
                        position: 'outside',
                        formatter: "{b|{b}}: \n{c} \n({d}%)",
                        rich:{
							b:{
								color: '#324e66',
								fontSize: 13,
		                        lineHeight: 20,
							}
						}
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
//                            fontSize: '30',
//                            fontWeight: 'bold'
                        },
                        position:'center'
                    }
                },
                labelLine:{
                	normal:{
                		lineStyle:{
                			color: '#D0D0D0'
                		}
                	}
                },
                itemStyle:{
                    normal:{
                        color: function(params) {
                            var colorList = [
                                '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                            ];
                            return colorList[params.dataIndex]
                        }
                    }
                },
                data: clientPay
            }
        ]
    };
    //myChart22.setOption(option22);

    //四大客群前五险种缴费分布
    var clientTops1 = [
        {name:'意外险',value:1500},
        {name:'寿险',value:1600},
        {name:'车险',value:2200},
        {name:'健康险',value:2500},
        {name:'养老险',value:2900}
    ];
    //修改图23所需数据格式
   $scope.changeData23 = function(data){
	    var insurances1 = [];
	    var number1 = [];
	    for(var i=data.length-1;i>=0;i--){
	        insurances1.push(data[i].CLASSNAME);
	        number1.push(data[i].COUNT_POLICYNO);
	    	//insurances1.push(data[i].name);
	        //number1.push(data[i].value);
	    }
	    console.log('insurances1', insurances1);
	    console.log('number1', number1);
	    $scope.option23 = {
	            backgroundColor:'#fff',
	            tooltip: {
	                trigger: 'axis',
	                padding: [5,10],
	                formatter: "{b} :  {c} ",
	                extraCssText: 'border-radius: 5px',
	                axisPointer:{
	                    type: 'shadow',
	                    shadowStyle:{
	                        opacity:0
	                    }
	                }
	            },
	            toolbox: {
	                show : true,
	                feature : {
	                    saveAsImage : {show: true}
	                },
	                bottom: 10,
	                right: 0
	            },
	            grid: {
	                left: '3%',
	                right: '5%',
	                bottom: '3%',
	                top: '3%',
	                containLabel: true
	            },
	            xAxis: {
	                type: 'value',
	                boundaryGap: [0, 0.01],
	                splitLine:{
	                    show:false
	                },
	                axisLine:{
	                    lineStyle:{
	                        color:'#9D9D9D'
	                    }
	                }
	            },
	            yAxis: {
	                type: 'category',
	                axisLine:{
	                    lineStyle:{
	                        color:'#9D9D9D'
	                    }
	                },
	                data: insurances1
	            },
	            series: [
	                {
	                    type: 'bar',
	                    label : {
							normal : {
								show : true,
								position : 'right',
								textStyle:{
									color: '#737373'
								}
							}
						},
	                    itemStyle:{
	                        normal:{
	                            color:'#6ca8ee',
	                            opacity:'0.8'
	                        },
	                        emphasis:{
	                            color:'#2493ca'
	                        }
	                    },
	                    barWidth:'50%',
	                    data: number1
	                }
	            ]
	        };
   }
    var myChart23 = echarts.init(document.getElementById('payment-bottom-right'));
   
    //myChart23.setOption(option23);
    
    var leads = [
        {name:'3年',value:1500},
        {name:'其他',value:600},
        {name:'10年15年',value:2200},
        {name:'五年',value:1200},
        {name:'趸交',value:900}
    ];
    var myChart24 = echarts.init(document.getElementById('payment-bottom-right2'));
    var option24 = {
        backgroundColor:'#fff',
        title: {
        	show: true,
        	text: 'title',
        	textStyle:{
        		color: '#6694cc',
        		fontSize: 14,
        		align: 'center'
        	},
        	top: 0,
        	left: 'center'
        },
        tooltip: {
        	show:false,
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
        toolbox: {
            show : true,
            feature : {
                saveAsImage : {show: true}
            },
            bottom: 10,
            right: 0
        },
        series: [
            {
                type:'pie',
                radius: ['40%', '70%'],
                avoidLabelOverlap: false,
                label: {
                    normal: {
                        show: true,
                        textStyle:{
                        	color: '#737373'
                        },
                        position: 'outside',
                        formatter: "{b|{b}}: \n{c} \n({d}%)",
                        rich:{
							b:{
								color: '#324e66',
								fontSize: 13,
		                        lineHeight: 20,
							}
						}
                    }
                },
                labelLine:{
                	normal:{
                		lineStyle:{
                			color: '#D0D0D0'
                		}
                	}
                },
                itemStyle:{
                    normal:{
                        color: function(params) {
                            var colorList = [
                                '#1e4b71','#d04840','#e69c40','#e9c647','#3486bb'
                            ];
                            return colorList[params.dataIndex]
                        }
                    }
                },
                data: []
            },
            {
				type : 'pie',
				radius : [ '40%', '70%' ],
				avoidLabelOverlap : false,
				label : {
					normal : {
						show : false,
						position : 'center',
						formatter : "{b} \n{c} \n({d}%)"
					},
					emphasis : {
						show : true,
						textStyle : {
							color: '#324e66',
							fontSize: 13
						},
						lineHeight: 26,
						formatter : "{b|{d}%} \n{b} {c}",
						rich:{
							b:{
								color: '#035eae',
								fontSize: 20
							}
						}
					}
				},
				itemStyle : {
					normal : {
						color : function(params) {
							var colorList = [ '#1e4b71', '#d04840', '#e69c40','#e9c647', '#3486bb',
							                  '#6B337E', '#9E3D83'];
							return colorList[params.dataIndex];
						}
					}
				},
				data : []
			}
        ]
    };
    //myChart24.setOption(option24);

    $(window).resize(function(){
        setTimeout(function(){
            myChart20.resize();
            myChart21.resize();
            myChart22.resize();
            myChart23.resize();
            myChart24.resize();
        },100);
    });
    
    var test_data = [
		{name:'意外险',value:3500},
		{name:'寿险',value:2600},
		{name:'车险',value:2200},
		{name:'健康险',value:1500},
		{name:'养老险',value:900}
    ];
    

	$scope.yiwaixian = [
	     {name:'三年',value:1500},
	     {name:'五年',value:600},
	     {name:'趸交',value:2200},
	     {name:'其他',value:500},
	     {name:'10年15年及以上',value:900}
	 ];
    var typeToPiyin = {"意外险":$scope.yiwaixian,"寿险":"shouxian","车险":"chexian","健康险":"jiankangxian","养老险":"yanglaoxian"}; 
    
    /**
	 * 获取数据
	 */
    $scope.startData = function(){
        $http({
			method:'post',
			url:context+'/paymentYearsAnalysis/allPaymentYearAnalysis',
			data:angular.toJson($scope.reqData)
		}).success(function(data){
			console.log('data', data);
			 $rootScope.excelData = data;
			//保单缴费年限分布
			option20.series[0].data=angular.fromJson(data.paymentYears);
			myChart20.setOption(option20);
			//前十主力险种缴费年限
			$scope.changeData21(angular.fromJson(data.topTenPaymentYear));
			myChart21.setOption($scope.option21);
			//四大客群主要缴费年限
			option22.series[0].data=angular.fromJson(data.fourTypePaymentYear);
			myChart22.setOption(option22);
			//四大客群前五缴费年限分布
			$scope.changeData23(angular.fromJson(data.fourTypeTopFive));
			myChart23.setOption($scope.option23);
			//四大客群前五缴费年限分布-联动饼图
			option24.title.text = "";
			option24.series[0].data=angular.fromJson(data.fourTypeTopFivePaymentYear);
			option24.series[1].data=angular.fromJson(data.fourTypeTopFivePaymentYear);
			myChart24.setOption(option24);
		});
    };
    $scope.startData();
  
    //获取联动饼图的数据
    $scope.getData = function(){
        $http({
			method:'post',
			url:context+'/paymentYearsAnalysis/getTopFive',
			data:angular.toJson($scope.reqData)
		}).success(function(data){
			console.log('获取联动饼图的数据', data);
			option24.series[0].data=angular.fromJson(data.fourTypeTopFivePaymentYear);
			myChart24.setOption(option24);
		});
    };
    myChart23.on('click', function (result) {
    	console.log('result', result);
    	$scope.reqData.classname = result.name;
    	option24.title.text = result.name;
    	$scope.getData();
        //option24.series[0].data = typeToPiyin[result.name];
		//myChart24.setOption(option24);
    });

    //四大客群下拉框操作
    $scope.clientGroup='阳光青年';
    $scope.choseGroup = function() {
    	$scope.reqData.personMark = $scope.clientGroup;
    	$scope.startData();
    };
});
