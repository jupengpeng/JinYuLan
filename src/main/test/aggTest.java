import static org.junit.Assert.*;

import java.util.List;

import org.elasticsearch.action.percolate.TransportShardMultiPercolateAction.Response;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.junit.Before;
import org.junit.Test;

import com.els.ElsConnection;
import com.els.connection.ConfigElastic;
import com.els.domain.QueryRespVO;
import com.google.gson.JsonObject;


public class aggTest {
	String indexName="";
	String type="";
	ElsConnection ec=new ConfigElastic("/config.properties");
	@Before
	public void before(){
		ec.buildClient();
	}
	@Test
	public void test() {
		String queryword="金融";
		QueryBuilder queryBuilder=QueryBuilders.matchQuery("title", queryword);
		AbstractAggregationBuilder aggregation=AggregationBuilders.terms("tagAgg").field("tag..raw").subAggregation(AggregationBuilders.terms("urlAgg").field("url"));
		SearchRequestBuilder request = ec.getClient().prepareSearch("pwc_webpage").setTypes("page").setQuery(queryBuilder).addAggregation(aggregation);
		SearchResponse response = request.execute().actionGet();
		System.out.println(response);
		
	}
	@Test
	public void searchTest(){
		Client client = ec.getClient();
		QueryBuilder queryBuilder = null;
		AbstractAggregationBuilder aggregation = null;
		SearchRequestBuilder request = client.prepareSearch(indexName)
		.setTypes(type)
		.setQuery(queryBuilder).setSize(10).setFrom(0)
		.addAggregation(aggregation);

		QueryRespVO qrv = ec.executeQueryRequest(request);
		List<JsonObject> result = qrv.getData();
		
		
	}

}
