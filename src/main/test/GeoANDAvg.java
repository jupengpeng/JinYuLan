import static org.junit.Assert.*;

import java.util.List;

import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.elasticsearch.search.aggregations.metrics.avg.Avg;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.els.ElsConnection;
import com.els.domain.QueryRespVO;
import com.google.gson.JsonObject;
import com.webapp.corecenter.ElasticFactory;


public class GeoANDAvg {
	ElsConnection ec;
	String indexName="cpicdp";
	String typeName="store";
	@Before
	public void before(){
		ec=ElasticFactory.getConnection();
	}
	//查询3KM之内的商家，包括本店
	@Test
	public void geo() {
		String id="5446138";
		JsonObject targetStore=ec.selectOne(indexName, typeName, id, null);
		//System.out.println(targetStore);
		Double lat=Double.parseDouble(targetStore.get("location").getAsJsonObject().get("lat").getAsString());
		Double lon=Double.parseDouble(targetStore.get("location").getAsJsonObject().get("lon").getAsString());
		FilterBuilder filterBuilder=FilterBuilders.geoDistanceFilter("location").distance("3km")
				.point(lat, lon);          
		QueryBuilder queryBuilder=QueryBuilders.filteredQuery(QueryBuilders.matchAllQuery(), filterBuilder);
		//System.out.println(queryBuilder);
		SearchRequestBuilder request = ec.prepareSearch(indexName, typeName, queryBuilder);
		QueryRespVO result = ec.executeQueryRequest(request);
		//System.out.println(result.getData());
		//System.out.println(result.getTotal());
	}
	//计算全市平均值
	@Test
	public void aggAvg(){
		QueryBuilder queryBuilder=QueryBuilders.matchAllQuery();
		SearchRequestBuilder request = ec.prepareSearch(indexName, typeName, queryBuilder);
		AbstractAggregationBuilder aggregation=AggregationBuilders.avg("commentNum").field("commentNum");
		request.addAggregation(aggregation);
		SearchResponse response = request.execute().actionGet();

/*		response.getAggregations().get("commentNum").toString();
		System.out.println("123");
		System.out.println(response.getAggregations().);*/

		//System.out.println(response);
		Avg agg = response.getAggregations().get("commentNum");
		
		System.out.println(agg.getValue());

	}
	

	@Test
	public void testAgg(){
		
		QueryBuilder queryBuilder = QueryBuilders.matchAllQuery();
		SearchRequestBuilder srb = ec.prepareSearch(indexName, typeName, queryBuilder);
		//srb.setSearchType(SearchType.COUNT);
		AbstractAggregationBuilder agg = AggregationBuilders.terms("district").field("district.raw");
		srb.addAggregation(agg);
		SearchResponse response = srb.execute().actionGet();
		response.getAggregations();
//		System.err.println(response);
		QueryRespVO qrv = ec.executeQueryRequest(srb);
		 List<JsonObject> list = qrv.getAgg("district");
		System.err.println(list);

		
	}



}
